<?php

/**
 * Class ControllerAccountAccount
 * @property Customer $customer
 * @property Loader $load
 * @property ModelAccountAddress $model_account_address
 * @property Document $document
 * @property ModelAccountOrder $model_account_order
 * @property Response $response
 */
class ControllerAccountAccount extends Controller
{

    private $error = [];

    public function index()
    {


        if (!$this->customer->isLogged()) {
            $this->session->data['redirect'] = $this->url->link('account/account', '', 'SSL');

            $this->response->redirect($this->url->link('account/login', '', 'SSL'));
        }

        $this->load->language('account/register');

//		$this->document->addScript('catalog/view/theme/denis_shop/js/common/indexPage.js');
//        $this->document->addScript('catalog/view/theme/denis_shop/js/common/pageRegister.js');
//        $this->document->addScript('catalog/view/theme/denis_shop/libs/jquery-mousewheel/jquery.mousewheel.min.js');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->editData();
        }

        $this->load->language('account/account');

        $this->document->setTitle($this->language->get('heading_title'));

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_account'),
            'href' => $this->url->link('account/account', '', 'SSL')
        );

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_my_account'] = $this->language->get('text_my_account');
        $data['text_my_orders'] = $this->language->get('text_my_orders');
        $data['text_my_newsletter'] = $this->language->get('text_my_newsletter');
        $data['text_edit'] = $this->language->get('text_edit');
        $data['text_password'] = $this->language->get('text_password');
        $data['text_address'] = $this->language->get('text_address');
        $data['text_wishlist'] = $this->language->get('text_wishlist');
        $data['text_order'] = $this->language->get('text_order');
        $data['text_download'] = $this->language->get('text_download');
        $data['text_reward'] = $this->language->get('text_reward');
        $data['text_return'] = $this->language->get('text_return');
        $data['text_transaction'] = $this->language->get('text_transaction');
        $data['text_newsletter'] = $this->language->get('text_newsletter');
        $data['text_recurring'] = $this->language->get('text_recurring');
        $data['text_select'] = $this->language->get('text_select');

        $this->load->model('localisation/country');
        $data['countries'] = $this->model_localisation_country->getCountries();
        $data['action'] = $this->url->link('account/account', '', 'SSL');

        $data['edit'] = $this->url->link('account/edit', '', 'SSL');
        $data['password'] = $this->url->link('account/password', '', 'SSL');
        $data['address'] = $this->url->link('account/address', '', 'SSL');
        $data['wishlist'] = $this->url->link('account/wishlist');
        $data['order_link'] = $this->url->link('account/order', '', 'SSL');
        $data['download'] = $this->url->link('account/download', '', 'SSL');
        $data['return'] = $this->url->link('account/return', '', 'SSL');
        $data['transaction'] = $this->url->link('account/transaction', '', 'SSL');
        $data['newsletter'] = $this->url->link('account/newsletter', '', 'SSL');
        $data['recurring'] = $this->url->link('account/recurring', '', 'SSL');

        if ($this->config->get('reward_status')) {
            $data['reward'] = $this->url->link('account/reward', '', 'SSL');
        } else {
            $data['reward'] = '';
        }

        $data = array_merge($data, $this->getOrdersInfo());
        $data = array_merge($data, $this->getWishListData());
        $data['rewards'] = $this->getRewardsData();


        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        //Errors block START
        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->error['firstname'])) {
            $data['error_firstname'] = $this->error['firstname'];
        } else {
            $data['error_firstname'] = '';
        }

        if (isset($this->error['lastname'])) {
            $data['error_lastname'] = $this->error['lastname'];
        } else {
            $data['error_lastname'] = '';
        }

        if (isset($this->error['email'])) {
            $data['error_email'] = $this->error['email'];
        } else {
            $data['error_email'] = '';
        }

        if (isset($this->error['telephone'])) {
            $data['error_telephone'] = $this->error['telephone'];
        } else {
            $data['error_telephone'] = '';
        }

        if (isset($this->error['address_1'])) {
            $data['error_address_1'] = $this->error['address_1'];
        } else {
            $data['error_address_1'] = '';
        }

        if (isset($this->error['city'])) {
            $data['error_city'] = $this->error['city'];
        } else {
            $data['error_city'] = '';
        }

        if (isset($this->error['postcode'])) {
            $data['error_postcode'] = $this->error['postcode'];
        } else {
            $data['error_postcode'] = '';
        }

        if (isset($this->error['country'])) {
            $data['error_country'] = $this->error['country'];
        } else {
            $data['error_country'] = '';
        }

        if (isset($this->error['zone'])) {
            $data['error_zone'] = $this->error['zone'];
        } else {
            $data['error_zone'] = '';
        }

        if (isset($this->error['password'])) {
            $data['error_password'] = $this->error['password'];
        } else {
            $data['error_password'] = '';
        }

        if (isset($this->error['confirm'])) {
            $data['error_confirm'] = $this->error['confirm'];
        } else {
            $data['error_confirm'] = '';
        }

        //Errors block END


        $data = array_merge($data, $this->getAccountInfo($this->customer));

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/account.tpl')) {
            $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/account.tpl', $data));
        } else {
            $this->response->setOutput($this->load->view('default/template/account/account.tpl', $data));
        }
    }

    public function country()
    {
        $json = array();

        $this->load->model('localisation/country');

        $country_info = $this->model_localisation_country->getCountry($this->request->get['country_id']);

        if ($country_info) {
            $this->load->model('localisation/zone');

            $json = array(
                'country_id' => $country_info['country_id'],
                'name' => $country_info['name'],
                'iso_code_2' => $country_info['iso_code_2'],
                'iso_code_3' => $country_info['iso_code_3'],
                'address_format' => $country_info['address_format'],
                'postcode_required' => $country_info['postcode_required'],
                'zone' => $this->model_localisation_zone->getZonesByCountryId($this->request->get['country_id']),
                'status' => $country_info['status']
            );
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function getOrdersPartial()
    {
        $data = $this->getOrdersInfo();

        $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/ordersInfoPartial.tpl', $data));
    }

    private function getRewardsData()
    {
        $data = [];

        $this->load->language('account/reward');

        $this->load->model('account/reward');


        $data['column_date_added'] = $this->language->get('column_date_added');
        $data['column_description'] = $this->language->get('column_description');
        $data['column_points'] = $this->language->get('column_points');

        $data['text_total'] = $this->language->get('text_total');
        $data['text_empty'] = $this->language->get('text_empty');

        $data['rewards'] = array();

        $filter_data = array(
            'sort' => 'date_added',
            'order' => 'DESC',
            'start' => 0,
            'limit' => 100000
        );

        $reward_total = $this->model_account_reward->getTotalRewards();

        $results = $this->model_account_reward->getRewards($filter_data);

        foreach ($results as $result) {
            $data['rewards'][] = array(
                'order_id' => $result['order_id'],
                'points' => $result['points'],
                'description' => $result['description'],
                'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
                'href' => $this->url->link('account/order/info', 'order_id=' . $result['order_id'], 'SSL')
            );
        }

        $data['total'] = (int)$this->customer->getRewardPoints();

        return $data;
    }

    private function getWishListData()
    {
        $data = [];

        $this->load->language('account/wishlist');

        $this->load->model('account/wishlist');

        $this->load->model('catalog/product');

        $this->load->model('tool/image');

        if (isset($this->request->get['remove'])) {
            // Remove Wishlist
            $this->model_account_wishlist->deleteWishlist($this->request->get['remove']);

            $this->session->data['success'] = $this->language->get('text_remove');
        }


        $data['text_empty'] = $this->language->get('text_empty');

        $data['products'] = array();

        $results = $this->model_account_wishlist->getWishlist();

        foreach ($results as $result) {
            $product_info = $this->model_catalog_product->getProduct($result['product_id']);

            if ($product_info) {
                if ($product_info['image']) {
                    $image = $this->model_tool_image->resize($product_info['image'], $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height'));
                } else {
                    $image = false;
                }

                if ($product_info['quantity'] <= 0) {
                    $stock = $product_info['stock_status'];
                } elseif ($this->config->get('config_stock_display')) {
                    $stock = $product_info['quantity'];
                } else {
                    $stock = $this->language->get('text_instock');
                }

                if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                    $price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
                } else {
                    $price = false;
                }

                if ((float)$product_info['special']) {
                    $special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
                } else {
                    $special = false;
                }
                if ($product_info['quantity'] <= 0) {
                    $stock = [
                        'text' => $product_info['stock_status'],
                        'class' => 'yes noCol'
                    ];

                } elseif ($this->config->get('config_stock_display')) {
                    $stock['text'] = $product_info['quantity'];
                } else {
                    $stock = [
                        'text' => $this->language->get('text_instock'),
                        'class' => 'yes',
                    ];
                }
                $data['products'][] = array(
                    'product_id' => $product_info['product_id'],
                    'thumb' => $image,
                    'name' => $product_info['name'],
                    'model' => $product_info['model'],
                    'stock' => $stock,
                    'price' => $price,
                    'special' => $special,
                    'href' => $this->url->link('product/product', 'product_id=' . $product_info['product_id']),
                    'remove' => $this->url->link('account/wishlist', 'remove=' . $product_info['product_id']),
                    'rating' => $product_info['rating'],
                    'stock_status' => $stock,
                );
            }
        }

        return $data;
    }

    private function getOrdersInfo()
    {
        $data = [];
        $data['telephone'] = $this->customer->getTelephone();
        //ORDERS START
        $this->load->model('account/order');

        if (!empty($this->request->get['order_sort'])) {
            $data['order_sort'] = (int)$this->request->get['order_sort'];
        } else {
            $data['order_sort'] = 0;
        }

        $results = $this->model_account_order->getOrders(
            0,
            1000,
            $this->getPastDateByDays($data['order_sort'])
        );

        foreach ($results as $result) {
            $product_total = $this->model_account_order->getTotalOrderProductsByOrderId($result['order_id']);
            $voucher_total = $this->model_account_order->getTotalOrderVouchersByOrderId($result['order_id']);

            $data['orders'][] = array(
                'order_id' => $result['order_id'],
                'name' => $result['firstname'] . ' ' . $result['lastname'],
                'status' => $result['status'],
                'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
                'products' => ($product_total + $voucher_total),
                'products_info' => $result['products'],
                'total' => $this->currency->format($result['total'], $result['currency_code'], $result['currency_value']),
                'href' => $this->url->link('account/order/info', 'order_id=' . $result['order_id'], 'SSL'),
            );
        }

        // Totals
        $data['totals'] = array();
        foreach ($results as $order) {
//            myDump($order);
            $totals = $this->model_account_order->getOrderTotals($order['order_id']);
            $full_order_info = $this->model_account_order->getOrder($order['order_id']);

            foreach ($totals as $total) {
                $data['totals'][$order['order_id']][] = array(
                    'title' => $total['title'],
                    'text' => $this->currency->format($total['value'], $order['currency_code'], $order['currency_value']),
                );
            }

            $format = '{country} {zone} {city} {postcode} {address_1}';

            $find = array(
                '{address_1}',
                '{city}',
                '{postcode}',
                '{zone}',
                '{zone_code}',
                '{country}'
            );

            $replace = array(
                'address_1' => $full_order_info['payment_address_1'],
                'city' => $full_order_info['payment_city'],
                'postcode' => $full_order_info['payment_postcode'],
                'zone' => $full_order_info['payment_zone'],
                'zone_code' => $full_order_info['payment_zone_code'],
                'country' => $full_order_info['payment_country']
            );

            $data['payment_address'][$order['order_id']] = str_replace(array("\r\n", "\r", "\n"), '<br />',
                preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />',
                    trim(str_replace($find, $replace, $format))));

            $data['order_status_id'][$order['order_id']] = $full_order_info['order_status_id'];
//            myDump($data['order_status_id']);
        }

        //ORDERS START

        return $data;
    }

    public function ajaxAccountInfo()
    {
        $this->load->model('account/address');
        $data = $this->getAccountInfo($this->customer);
        $data['address'] = $this->model_account_address->getAddress($this->customer->getAddressId());


        $this->load->language('checkout/checkout');

        if (isset($this->session->data['shipping_address'])) {
            // Shipping Methods
            $method_data = array();

            $this->load->model('extension/extension');

            $results = $this->model_extension_extension->getExtensions('shipping');

            foreach ($results as $result) {
                if ($this->config->get($result['code'] . '_status')) {
                    $this->load->model('shipping/' . $result['code']);

                    $quote = $this->{'model_shipping_' . $result['code']}->getQuote($this->session->data['shipping_address']);

                    if ($quote) {
                        $method_data[$result['code']] = array(
                            'title' => $quote['title'],
                            'quote' => $quote['quote'],
                            'sort_order' => $quote['sort_order'],
                            'error' => $quote['error']
                        );
                    }
                }
            }

            $sort_order = array();

            foreach ($method_data as $key => $value) {
                $sort_order[$key] = $value['sort_order'];
            }

            array_multisort($sort_order, SORT_ASC, $method_data);

            $this->session->data['shipping_methods'] = $method_data;
        }

        $data['text_shipping_method'] = $this->language->get('text_shipping_method');
        $data['text_comments'] = $this->language->get('text_comments');
        $data['text_loading'] = $this->language->get('text_loading');

        $data['button_continue'] = $this->language->get('button_continue');

        if (empty($this->session->data['shipping_methods'])) {
            $data['error_warning'] = sprintf($this->language->get('error_no_shipping'), $this->url->link('information/contact'));
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['shipping_methods'])) {
            $data['shipping_methods'] = $this->session->data['shipping_methods'];
        } else {
            $data['shipping_methods'] = array();
        }

        if (isset($this->session->data['shipping_method']['code'])) {
            $data['code'] = $this->session->data['shipping_method']['code'];
        } else {
            $data['code'] = '';
        }

        if (isset($this->session->data['comment'])) {
            $data['comment'] = $this->session->data['comment'];
        } else {
            $data['comment'] = '';
        }


        $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/accountInfoPartial.tpl', $data));
    }

    public function ajaxEditAccountInfo()
    {
        $this->load->language('account/register');

        $data = [
            'result' => 'failed',
        ];

        if ($this->customer->isLogged() && $this->request->server['REQUEST_METHOD'] == 'POST' && $this->validateCustomerData()) {
            $this->editCustomerData();
            $data['result'] = 'success';
        }

        //Errors block START
        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->error['firstname'])) {
            $data['error_firstname'] = $this->error['firstname'];
        } else {
            $data['error_firstname'] = '';
        }

        if (isset($this->error['lastname'])) {
            $data['error_lastname'] = $this->error['lastname'];
        } else {
            $data['error_lastname'] = '';
        }

        if (isset($this->error['email'])) {
            $data['error_email'] = $this->error['email'];
        } else {
            $data['error_email'] = '';
        }

        if (isset($this->error['telephone'])) {
            $data['error_telephone'] = $this->error['telephone'];
        } else {
            $data['error_telephone'] = '';
        }

        if (isset($this->error['password'])) {
            $data['error_password'] = $this->error['password'];
        } else {
            $data['error_password'] = '';
        }

        if (isset($this->error['confirm'])) {
            $data['error_confirm'] = $this->error['confirm'];
        } else {
            $data['error_confirm'] = '';
        }

        //Errors block END
        myDump($this->error);
        $this->response->setOutput(json_encode($data));
    }

    private function getAccountInfo(Customer $customer)
    {
        $this->load->model('account/address');
        $address = $this->model_account_address->getAddress($customer->getAddressId());
        $data = [
            'firstname' => $customer->getFirstName(),
            'lastname' => $customer->getLastName(),
            'telephone' => $customer->getTelephone(),
            'email' => $customer->getEmail(),
            'birth_date' => $customer->getBirthDate(),
            'country_id' => $address['country_id'],
            'zone_id' => $address['zone_id'],
            'city' => $address['city'],
            'postcode' => $address['postcode'],
            'address_1' => $address['address_1'],
            'address_id' => $customer->getAddressId(),
        ];


        return $data;
    }

    private function editData()
    {
        $this->editCustomerData();
        $this->editCustomerAddress();

        $this->response->redirect($this->url->link('account/account', '', 'SSL'));
    }

    private function editCustomerData()
    {
        $this->load->model('account/customer');

        $this->model_account_customer->editCustomer($this->request->post);

        if (!empty($this->request->post['password'])) {
            $this->model_account_customer->editPassword($this->customer->getEmail(), $this->request->post['password']);
        }

        // Add to activity log
        $this->load->model('account/activity');

        $activity_data = array(
            'customer_id' => $this->customer->getId(),
            'name' => $this->customer->getFirstName() . ' ' . $this->customer->getLastName()
        );

        $this->model_account_activity->addActivity('edit', $activity_data);
    }

    private function editCustomerAddress()
    {
        $this->load->model('account/address');
        $this->model_account_address->editAddress($this->request->post['address_id'], $this->request->post);

        // Default Shipping Address
        if (isset($this->session->data['shipping_address']['address_id']) && ($this->request->post['address_id'] == $this->session->data['shipping_address']['address_id'])) {
            $this->session->data['shipping_address'] = $this->model_account_address->getAddress($this->request->post['address_id']);

            unset($this->session->data['shipping_method']);
            unset($this->session->data['shipping_methods']);
        }

        // Default Payment Address
        if (isset($this->session->data['payment_address']['address_id']) && ($this->request->post['address_id'] == $this->session->data['payment_address']['address_id'])) {
            $this->session->data['payment_address'] = $this->model_account_address->getAddress($this->request->post['address_id']);

            unset($this->session->data['payment_method']);
            unset($this->session->data['payment_methods']);
        }

        if (!empty($this->request->post['password'])) {
            $this->model_account_customer->editPassword($this->customer->getEmail(), $this->request->post['password']);
        }

        // Add to activity log
        $this->load->model('account/activity');

        $activity_data = array(
            'customer_id' => $this->customer->getId(),
            'name' => $this->customer->getFirstName() . ' ' . $this->customer->getLastName()
        );

        $this->model_account_activity->addActivity('edit', $activity_data);
    }

    private function validate()
    {
        $this->validateCustomerData();
        $this->validateCustomerAddress();
        return empty($this->error);
    }

    private function validateCustomerData()
    {
        $this->load->model('account/customer');
        $this->load->model('localisation/country');

        if ((utf8_strlen(trim($this->request->post['firstname'])) < 1) || (utf8_strlen(trim($this->request->post['firstname'])) > 32)) {
            $this->error['firstname'] = $this->language->get('error_firstname');
        }

        if ((utf8_strlen(trim($this->request->post['lastname'])) < 1) || (utf8_strlen(trim($this->request->post['lastname'])) > 32)) {
            $this->error['lastname'] = $this->language->get('error_lastname');
        }


        if ($this->customer->getEmail() != $this->request->post['email']) {
            if ((utf8_strlen($this->request->post['email']) > 96) || !preg_match($this->config->get('config_mail_regexp'), $this->request->post['email'])) {
                $this->error['email'] = $this->language->get('error_email');
            }

            if ($this->model_account_customer->getTotalCustomersByEmail($this->request->post['email'])) {
                $this->error['warning'] = $this->language->get('error_exists');
            }
        }


        if ((utf8_strlen($this->request->post['telephone']) < 3) || (utf8_strlen($this->request->post['telephone']) > 32)) {
            $this->error['telephone'] = $this->language->get('error_telephone');
        }

        if ($this->customer->getTelephone() != $this->request->post['telephone']) {
            if ((utf8_strlen($this->request->post['telephone']) < 3) || (utf8_strlen($this->request->post['telephone']) > 32)) {
                $this->error['telephone'] = $this->language->get('error_telephone');
            }

            if ($this->model_account_customer->getTotalCustomersByTelephone($this->request->post['telephone'])) {
                $this->error['telephone'] = $this->language->get('telephone_exists');
            }
        }

        if (!empty($this->request->post['password'])) {
            if ((utf8_strlen($this->request->post['password']) < 4) || (utf8_strlen($this->request->post['password']) > 20)) {
                $this->error['password'] = $this->language->get('error_password');
            }

            if ($this->request->post['confirm'] != $this->request->post['password']) {
                $this->error['confirm'] = $this->language->get('error_confirm');
            }
        }

        return empty($this->error);
    }

    private function validateCustomerAddress()
    {
        $this->load->model('localisation/country');

        if ((utf8_strlen(trim($this->request->post['address_1'])) < 3) || (utf8_strlen(trim($this->request->post['address_1'])) > 128)) {
            $this->error['address_1'] = $this->language->get('error_address_1');
        }

        if ((utf8_strlen(trim($this->request->post['city'])) < 2) || (utf8_strlen(trim($this->request->post['city'])) > 128)) {
            $this->error['city'] = $this->language->get('error_city');
        }

        $this->load->model('localisation/country');

        $country_info = $this->model_localisation_country->getCountry($this->request->post['country_id']);

        if ($country_info && $country_info['postcode_required'] && (utf8_strlen(trim($this->request->post['postcode'])) < 2 || utf8_strlen(trim($this->request->post['postcode'])) > 10)) {
            $this->error['postcode'] = $this->language->get('error_postcode');
        }

        if ($this->request->post['country_id'] == '') {
            $this->error['country'] = $this->language->get('error_country');
        }

        if (!isset($this->request->post['zone_id']) || $this->request->post['zone_id'] == '' || !is_numeric($this->request->post['zone_id'])) {
            $this->error['zone'] = $this->language->get('error_zone');
        }

        return empty($this->error);
    }

    private function getPastDateByDays($daysPast)
    {
        if (empty($daysPast)) {
            return '';
        }

        $date = new DateTime();
        $date->setTimestamp($date->getTimestamp() - 60 * 60 * 24 * (int)$daysPast);

        return $date->format('Y-m-d H:i:s');
    }
}
