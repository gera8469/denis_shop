<?php

/**
 * Class ControllerCommonHome
 *
 * @property ModelCatalogCategory $model_catalog_category
 * @property Config $config
 * @property Document $document
 */
class ControllerCommonHome extends Controller {
	public function index() {
		$this->document->setTitle($this->config->get('config_meta_title'));
		$this->document->setDescription($this->config->get('config_meta_description'));
		$this->document->setKeywords($this->config->get('config_meta_keyword'));

        $this->document->addScript('/catalog/view/theme/denis_shop/js/common/indexPage.js', 'footer');
        $this->document->addStyle('/catalog/view/theme/denis_shop/bower_components/slick-carousel/slick/slick.css');
        $this->document->addStyle('/catalog/view/theme/denis_shop/bower_components/slick-carousel/slick/slick-theme.css');

        $data['categories'] = $this->getCategoriesInfoByParentId(0);


		if (isset($this->request->get['route'])) {
			$this->document->addLink(HTTP_SERVER, 'canonical');
		}

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/home.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/common/home.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/common/home.tpl', $data));
		}
	}

    private function getCategoriesInfoByParentId($parent_id)
    {
        $this->load->model('catalog/category');
        $this->load->model('tool/image');

        $categoriesInfo = $this->model_catalog_category->getCategories($parent_id);

        $categories = [];
        foreach ($categoriesInfo as $categoryInfo) {
            $categories[$categoryInfo['category_id']] = [
                'category_id' => $categoryInfo['category_id'],
                'name' => $categoryInfo['name'],
                'thumb' => '',
                'href'        => $this->url->link('product/category', 'path=' . $categoryInfo['category_id']),

            ];

            if ($categoryInfo['image']) {
                $categories[$categoryInfo['category_id']]['thumb'] = $this->model_tool_image->resize($categoryInfo['image'], $this->config->get('config_image_category_width'), $this->config->get('config_image_category_height'));
            }
        }

        return $categories;

    }
}