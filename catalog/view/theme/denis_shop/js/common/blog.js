//slick slider Popular
$(".slicBlog").slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    prevArrow: '<div class="prew1"><i class="fa fa-angle-left" aria-hidden="true"></i></div>',
    nextArrow: '<div class="next1"><i class="fa fa-angle-right" aria-hidden="true"></i></div>',
    dots: true,
    arrows: true
});
