$(document).ready(function () {


    // colors
    $('.js-color').on('click', function () {
        $(this).addClass('js-colorActive').siblings().removeClass('js-colorActive');
        var val = $(this).text();
        $('.colorItem__kol').text(val);
    });

    //скрипт для запрета перетяжки картинок
    $("img, a").on("dragstart", function (event) {
        event.preventDefault();
    });

    // preloader
    $(window).load(function () {
        $(".loader_inner").fadeOut();
        $(".loader").delay(400).fadeOut("slow");

    });

// top galery
//     $('.navMainSlidershow li:last-child').addClass('active');
    /*function handlerSliderShow(e) {
        e.preventDefault();
        var $this = $(this),
            item = $this.closest('.navMainSlidershow li'),
            container = $this.closest('.blocSlidershow'),
            display = container.find('.mainBlocImg'),
            paht = item.find('img').attr('src'),
            duration = 400;

        if (!item.hasClass('active')) {
            item.addClass('active').siblings().removeClass('active');

            display.find('img').fadeOut(duration, function () {
                $(this).attr('src', paht).fadeIn(duration);
            })
        }
    }*/

    // $('.navMainSlidershow__aImg').click(handlerSliderShow);

    var isExpanded = true;
    //для меню top
    $(".topHedLeft__bars").click(function () {
        if(isExpanded) {
            $(".clikMenu").slideUp();
            isExpanded = false;
        }
        else {
            $(".clikMenu").slideDown();
            isExpanded = true;
        }
        // $(".clikMenu").slideToggle();
        // $(".topHedLeft__bars i").toggleClass("active-bars");
    });

    // Dropdown Menu Fade
    jQuery(document).ready(function () {
        $(".dropdown").hover(
            function () {
                $('.dropdown-menu', this).fadeIn("fast");
            },
            function () {
                $('.dropdown-menu', this).fadeOut("fast");
            });
    });

    // для меню при мобильном
    function windowSize() {
        if ($(window).width() <= '500') {
            $(".dropdown").click(function () {
                $(".dropdown-menu").slideToggle();
                $(".dropdow a").toggleClass("active-bars");
            });
        }
    }

    $(window).on('load resize', windowSize);


    
    //hewer item
    $(".Item").hover(function (e) {
        e.preventDefault()
    });

    // Форма в модальном окне с фокусом на поле Имя
    $('.popup-with-form').magnificPopup({
        type: 'inline',
        focus: '#name'
    });
    $('.popup-with-form3').magnificPopup({
        type: 'inline',
        mainClass: 'mainPop',
        focus: '#name'
    });
    $('.modal3').find('.mfp-content').addClass('mfp-content2');

    // open/close popup callbeck (open)
    $('.callbec__calbec').on('click', function (e) {
        e.preventDefault();
        $('#callBacOpen').show('slow');
    });
    // (close)
    $(document).mouseup(function (e) {
        var container = $("#callBacOpen");
        if (container.has(e.target).length === 0){
            container.hide('slow');
        }
    });

    //close popup callback in esc
    if ($("#callBacOpen")) {
        $(this).keydown(function (eventObject) {
            if (eventObject.which == 27)
                $('#callBacOpen').hide('slow');
        });
    }

    
});
// mask popup callbac

jQuery(function ($) {
    $("#callBackphone").mask("(999) 999 99 99", {placeholder: " "});
});


// validation popup callbac
$(document).ready(function () {

    $("#callBacOpenForm").validate({

        rules: {

            callBacName: {
                required: true
            },

            callBacPhone: {
                required: true
            }

        },

        messages: {

            callBacName: {
                required: ""
            },

            callBacPhone: {
                required: ""
            }

        }

    });

});

 // попап при выходе
(function($) {
    $(function() {

        // Проверим, есть ли запись в куках о посещении посетителя
        // Если запись есть - ничего не делаем
        if (!$.cookie('was')) {

            $(document).mousemove(function (e) {

                if (e.pageY <= 5) {
                    $.magnificPopup.open({
                        items: {
                            src: '#form-popup2',
                            type: 'inline',
                            focus: '#name'
                        }
                    });
                }
            });

        }

        // Запомним в куках, что посетитель к нам уже заходил
        $.cookie('was', true, {
            expires: 1,
            path: '/'
        });

    })
})(jQuery);

//nicescroll
function handlerSliderShow(e) {
    e.preventDefault();
    var $this = $(this),
        item = $this.closest('.navMainSlidershow li'),
        container = $this.closest('.blocSlidershow'),
        display = container.find('.mainBlocImg'),
        paht = item.find('img').attr('src'),
        duration = 400;

    if (!item.hasClass('active')) {
        item.addClass('active').siblings().removeClass('active');

        display.find('img').fadeOut(duration, function () {
            $(this).attr('src', paht).fadeIn(duration);
        })
    }
}