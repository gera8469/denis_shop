$(document).ready(function () {
    //main slider
    $('.mainSlider').slick({
        autoplay: true,
        autoplaySpeed: 9000,
        speed: 900,
        arrows: false,
        dots: true,
        responsive: [
            {
                breakpoint: 640,
                settings: {
                    dots: false
                }

            }
        ]
    });


    // slick slider
    $('.slicSlider').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        prevArrow: '<div class="prew1"><i class="fa fa-angle-left" aria-hidden="true"></i></div>',
        nextArrow: '<div class="next1"><i class="fa fa-angle-right" aria-hidden="true"></i></div>',
        dots: true,
        arrows: true,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 3,
                    infinite: true
                }
            },
            {
                breakpoint: 720,
                settings: {
                    slidesToShow: 1,
                    dots: false,
                    infinite: true
                }
            },
            {
                breakpoint: 630,
                settings: {
                    slidesToShow: 1,
                    arrows: false,
                    autoplay: true,
                    dots: false,
                    autoplaySpeed: 7000,
                    touchMove: true
                }
            }
        ]
    });
    //slick slider Sale
    $(".slicSale").slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        prevArrow: '<div class="prew1"><i class="fa fa-angle-left" aria-hidden="true"></i></div>',
        nextArrow: '<div class="next1"><i class="fa fa-angle-right" aria-hidden="true"></i></div>',
        dots: true,
        centerMode: false,
        centerPadding: '10',
        arrows: true,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3,
                    infinite: true
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                    dots: false,
                    infinite: true
                }
            },
            {
                breakpoint: 700,
                settings: {
                    slidesToShow: 1,
                    autoplay: true,
                    dots: false,
                    autoplaySpeed: 7000,
                    touchMove: true
                }
            },
            {
                breakpoint: 500,
                settings: {
                    slidesToShow: 1,
                    autoplay: true,
                    arrows: false,
                    dots: false,
                    autoplaySpeed: 7000,
                    touchMove: true
                }
            }
        ]
    });

    //slick slider Popular
    $(".slicPopular").slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        prevArrow: '<div class="prew1"><i class="fa fa-angle-left" aria-hidden="true"></i></div>',
        nextArrow: '<div class="next1"><i class="fa fa-angle-right" aria-hidden="true"></i></div>',
        dots: false,
        centerMode: true,
        centerPadding: '10',
        arrows: true,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 2,
                    infinite: true
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                    dots: false,
                    infinite: true
                }
            },
            {
                breakpoint: 700,
                settings: {
                    slidesToShow: 1,
                    autoplay: true,
                    dots: false,
                    autoplaySpeed: 7000,
                    touchMove: true
                }
            },
            {
                breakpoint: 500,
                settings: {
                    slidesToShow: 1,
                    autoplay: true,
                    arrows: false,
                    dots: false,
                    autoplaySpeed: 7000,
                    touchMove: true
                }
            }
        ]
    });


});

// style select
/*(function ($) {
    $(function () {
        $('input,select').styler();
    });
})(jQuery);*/

