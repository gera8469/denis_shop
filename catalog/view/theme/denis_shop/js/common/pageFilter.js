// polzunok
$(document).ready(function () {
    $("#slider").slider({
        range: true,
        values: [1, 10000],
        step: 10,
        min: 0,//Минимально возможное значение на ползунке
        max: 10000,//Максимально возможное значение на ползунке
        create: function (event, ui) {
            val = $("#slider").slider("values", 0);//При создании слайдера, получаем его значение в перемен. val
            $("#otRes").html(val);//Заполняем этим значением элемент с id contentSlider

            val2 = $("#slider").slider("values", 1);//При создании слайдера, получаем его значение в перемен. val
            $("#doRes").html(val2);//Заполняем этим значением элемент с id contentSlider
        },
        slide: function (event, ui) {
            $("#otRes").html(ui.values[0]);//При изменении значения ползунка заполняем элемент с id contentSlider
            $("#doRes").html(ui.values[1]);
            $('#ot').val(ui.values[0]);
            $('#do').val(ui.values[1]);
        }
    });

});

var priceOt = document.getElementById('ot');
priceOt.oninput = function () {
    document.getElementById('otRes').innerHTML = priceOt.value;
};

var priceDo = document.getElementById('do');
priceDo.oninput = function () {
    document.getElementById('doRes').innerHTML = priceDo.value;
};


$(".addNavSidebar-js").click(function () {
    $(".sidebarNav2").slideToggle();
    $(this).toggleClass('pos');
    $(".addNavSidebar-js span").toggleClass('trans');
});
$(".addFilterSidebar-js").click(function () {
    $(".sidebarFilter").slideToggle();
    $(this).toggleClass('pos');
    $(".addFilterSidebar-js span").toggleClass('trans');
});
$(".filterOpen").click(function () {
    $(".aside").slideToggle();
    $(".filterOpen span").toggleClass('filterOpSpan');
});

// style select
(function ($) {
    $(function () {
        $('input,select').styler();
    });
})(jQuery);


