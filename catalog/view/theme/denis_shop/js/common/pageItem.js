$(document).ready(function () {
   
    

    // tabs
    function handlerTabs(e) {
        e.preventDefault();
        var item = $(this).closest('.blockTabsTabs__controls'),
            content = $('.tab'),
            itemPosition = item.index();

        content.eq(itemPosition)
            .add(item)
            .addClass('activeTab')
            .siblings()
            .removeClass('activeTab')
    }

    $('.blockTabsTabs__controls a').click(handlerTabs);

    //popup
    function handlerPopupComentsShow(e) {
        e.preventDefault();
        $(".comentsPopUp").toggle(function () {
                //Отображаем скрытый блок
                $(this).show('slow');
                return false;
            },
            function () {
                //Прячем скрытый блок
                $("this").hide('slow');
                return false;
            });//end of toggle()
    }

    $('#coments').click(handlerPopupComentsShow);

    $('.closePopup').click(handlerPopupComentsShow);

    if ($(".comentsPopUp")) {
        $(this).keydown(function (eventObject) {
            if (eventObject.which == 27)
                $('.comentsPopUp').hide();
        });
    }

    //slick slider Popular
    $(".slicPopular").slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        prevArrow: '<div class="prew1"><i class="fa fa-angle-left" aria-hidden="true"></i></div>',
        nextArrow: '<div class="next1"><i class="fa fa-angle-right" aria-hidden="true"></i></div>',
        dots: false,
        centerMode: true,
        centerPadding: '10',
        arrows: true,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 2,
                    infinite: true
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                    dots: false,
                    infinite: true
                }
            },
            {
                breakpoint: 700,
                settings: {
                    slidesToShow: 1,
                    autoplay: true,
                    dots: false,
                    autoplaySpeed: 7000,
                    touchMove: true
                }
            },
            {
                breakpoint: 500,
                settings: {
                    slidesToShow: 1,
                    autoplay: true,
                    arrows: false,
                    dots: false,
                    autoplaySpeed: 7000,
                    touchMove: true
                }
            }
        ]
    });

});

// style select
(function ($) {
    $(function () {
        $('select').styler();
    });
})(jQuery);