

jQuery(function ($) {
    $("#phone").mask("(999) 999 99 99", {placeholder: " "});
    $("#tin").mask("99-9999999");
    $("#ssn").mask("999-99-9999");
});

$(document).ready(function () {

    $("#formRegister").validate({

        rules: {

            name: {
                required: true
            },

            firstName: {
                required: true
            },
            phone: {
                required: true
            },
            email: {
                required: true,
                email: true
            },
            street: {
                required: true
            },
            home: {
                required: true
            },
            pass: {
                required: true
            },
            passContinue: {
                required: true,
                equalTo: "#pass"
            },
        },

        messages: {

            name: {
                required: ""
            },

            firstName: {
                required: ""
            },
            phone: {
                required: ""
            },
            email: {
                required: ""
            },
            street: {
                required: ""
            },
            home: {
                required: ""
            },
            pass: {
                required: ""
            },
            passContinue: {
                required: ""
            }

        }

    });

});

/*$(document).ready(function (){

    if($("#deliveri").val() == 1){
        $('#street').show("slow");
        $('#home').show("slow");
        $('#kvart').show("slow");
        $('#metro').show("slow");
        $('.formRegister__inpForm--none').hide("slow");
    }else{
        $('#street').hide("slow").removeAttr('required');
        $('#home').hide("slow").removeAttr('required');
        $('#kvart').hide("slow").removeAttr('required');
        $('#metro').hide("slow").removeAttr('required');
        $('.formRegister__inpForm--none').show("slow");
    }

    //select registration
    function handlerSelect() {
        var deliveri = $("#deliveri").val();
        console.log(deliveri);
        if(deliveri == 2){
            $('#street').hide("slow").removeAttr('required');
            $('#home').hide("slow").removeAttr('required');
            $('#kvart').hide("slow").removeAttr('required');
            $('#metro').hide("slow").removeAttr('required');
            $('.formRegister__inpForm--none').show("slow");
        }else{
            $('#street').show("slow");
            $('#home').show("slow");
            $('#kvart').show("slow");
            $('#metro').show("slow");
            $('.formRegister__inpForm--none').hide("slow");
        }
    }
    $('#deliveri').change(handlerSelect);
});*/

(function ($) {
    $(function () {
        // $('input, select').styler();
        $('#input-country').selectmenu();
    });
})(jQuery);