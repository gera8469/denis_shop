// tabs
function handlerTabs(e) {
  e.preventDefault();
  var item = $(this).closest('.blockTabsTabs__con'),
      content = $('.tab'),
      itemPosition = item.index();

  content.eq(itemPosition)
      .add(item)
      .addClass('activeTab')
      .siblings()
      .removeClass('activeTab');

  if ($('.blockTabsTabs__con--color').hasClass('activeTab')) {
    $('.blockTabsTabs__con--color').addClass('activeTab2');
  } else {
    $('.blockTabsTabs__con--color').removeClass('activeTab2');
  }
}

$('.blockTabsTabs__con a').click(handlerTabs);

//podsk
$(document).ready(function () {
  $('.wrapTitle__qwestions').hover(
      function () {
        $('.podskazka').show('slow');
      },
      function () {
        $('.podskazka').hide('slow');
      }
  )
});

//basked scroll

$(document).ready(
    function () {
      $(".wrapHideContent").niceScroll({
        horizrailenabled: false,
        cursorcolor: "#494e51",
        cursorwidth: "8px",
        cursorborder: "none",
        cursorborderradius: "0",
        background: "#e8e8e8",
        cursoropacitymin: "1"
      });
    }
);

// form 1
$('#formClient').on('change', function () {
  var del = $('#deliveri').val();
  var sit = $('#sity').val();
  var otd = $('#poshtN').val();
  var street = $('#street').val().length;
  var home = $('#home').val().length;

  if (del == 2) {
    $("#oform").attr('disabled', 'disabled');
    if (sit > 0 && otd > 0) {
      $("#oform").removeAttr('disabled');
      $('#street').removeAttr('required');
      $('#home').removeAttr('required');

      $('.one').removeClass('wrForm1__span--active');
      $('.two').addClass('wrForm1__span--active');
    }
  }

  if (del == 1) {
    $('#street').attr('required', 'required');
    $('#home').attr('required', 'required');
    if (street > 2 && home > 2) {
      $('.one').removeClass('wrForm1__span--active');
      $('.two').addClass('wrForm1__span--active');
    }
  }
});

//form 2
$("#oform2").attr('disabled', 'disabled');
$("#phoneNumber").attr('disabled', 'disabled');
$("#email").attr('disabled', 'disabled');

$('#formClient1').on('change', function () {
  var del = $('#name').val().length;
  var sit = $('#firstName').val().length;
  if (del > 2 && sit > 2) {

    $("#oform2").removeAttr('disabled');
    $("#phoneNumber").removeAttr('disabled');
    $("#email").removeAttr('disabled');

    $('.one').removeClass('wrForm1__span--active');
    $('.two').addClass('wrForm1__span--active');
  }
});
$('#formClient1').on('submit', function () {
  $.ajax({
    data: $("#oform2").serialize()
  }).done(function () {
    $('#formClient1').hide('slow');
    $('#formClient').show('slow');


  });
  return false;

});

$('.formEnter').on('submit', function () {
  $.ajax({
    type: 'POST',
    url: '/index.php?route=account/login/ajaxLogin&additionalData=cart',
    data: $("#formEnter").serialize()
  }).done(function (jsonResponse) {
    var responseObj = $.parseJSON(jsonResponse);

    if (responseObj.isLogged) {
      $('#checkout-cart').load('/index.php?route=checkout/confirm/customConfirm');
      $('.formEnterOld1').hide('slow');
      $('.formEnterOld2').show('slow');

      $('#wrapClient1').hide('slow');
      $('#wrapClient2').show('slow').css("display", "flex");
    }

  });
  return false;

});

//mask
jQuery(function ($) {
  $("#phoneNumber").mask("(999) 999 99 99", {placeholder: " "});
  $("#date").mask("99.99.9999", {placeholder: " "});

});

//validation
$(document).ready(function () {
  $("#formClient1").validate({
    rules: {

      name: {
        required: true
      },

      firstName: {
        required: true
      },
      phone: {
        required: true
      },
      email: {
        required: true,
        email: true
      },
      street: {
        required: true
      },
      home: {
        required: true
      },
      pass: {
        required: true
      },
      passContinue: {
        required: true,
        equalTo: "#pass"
      }
    },

    messages: {

      name: {
        required: ""
      },

      firstName: {
        required: ""
      },
      phone: {
        required: ""
      },
      email: {
        required: ""
      },
      street: {
        required: ""
      },
      home: {
        required: ""
      },
      pass: {
        required: ""
      },
      passContinue: {
        required: ""
      }

    }

  });

});

//form popup
$(document).ready(function () {

  if ($("#deliveri2").val() == 1) {
    $('#street2').show("slow");
    $('#home2').show("slow");
    $('#kvart2').show("slow");
    $('#metro2').show("slow");
    $('.formRegister__inpForm--none').hide("slow");
  } else {
    $('#street2').hide("slow").removeAttr('required');
    $('#home2').hide("slow").removeAttr('required');
    $('#kvart2').hide("slow").removeAttr('required');
    $('#metro2').hide("slow").removeAttr('required');
    $('.formRegister__inpForm--none').show("slow");
  }

  //select registration
  function handlerSelect() {
    var deliveri = $("#deliveri2").val();
    if (deliveri == 2) {
      $('#street2').hide("slow").removeAttr('required');
      $('#home2').hide("slow").removeAttr('required');
      $('#kvart2').hide("slow").removeAttr('required');
      $('#metro2').hide("slow").removeAttr('required');
      $('.formRegister__inpForm--none').show("slow");
    } else {
      $('#street2').show("slow");
      $('#home2').show("slow");
      $('#kvart2').show("slow");
      $('#metro2').show("slow");
      $('.formRegister__inpForm--none').hide("slow");
    }
  }

  $('.formPop2').change(handlerSelect);


});

$('#order-form').bind('click', customOrderSubmit);

function customOrderSubmit() {
  $.ajax({
    url: '/index.php?route=checkout/confirm/customConfirmSave',
    type: 'POST',
    data: $('#formClient1').serialize() + '&' + $('#formClient').serialize(),
    success: function (jsonResponse) {

      var objResponse = JSON.parse(jsonResponse);
      if (objResponse.result === 'success') {
        window.location = '/index.php?route=checkout/success'
      }
    }
  })
}

function Validator() {
  this.length = function (textInput, min, max) {
    var input = $(textInput);
    var value = input.val() + '';
    if (!value || value.length < min || value.length > max) {
      input.css('outline', 'red solid 1px');
    } else {
      input.css('outline', 'none');
    }
  };
  this.numbers = function (textInput) {
    var input = $(textInput);
    var value = +input.val();
    if (!value) {
      input.css('outline', 'red solid 1px');
    } else {
      input.css('outline', 'none');
    }
  }
}


var validator = new Validator();