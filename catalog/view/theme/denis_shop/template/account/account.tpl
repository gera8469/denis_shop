<?php echo $header; ?>

<section>
    <h1 class="tittleZacaz">личный кабинет</h1>
    <div class="zacazContainer">
        <div class="container">
            <div class="row">
                <div class="wrapClient wrapClient--borNo">
                    <ul class="blockTabsTabs blockTabsTabs2">
                        <li class="blockTabsTabs__controls activeTab activeTab2"><a href="#">Контактная информация
                                <div class='borderActive borderActive2'></div>
                            </a></li>
                        <li class="blockTabsTabs__controls"><a href="#">закладки
                                <div class='borderActive'></div>
                            </a></li>
                        <li class="blockTabsTabs__controls "><a href="#">история заказов
                                <div class='borderActive'></div>
                            </a></li>
                        <li class="blockTabsTabs__controls "><a href="#">мои бонусы
                                <div class='borderActive'></div>
                            </a></li>
                        <li class="blockTabsTabs__controls-exit "><a href="/index.php?route=account/logout">ВЫХОД</a>
                        </li>
                    </ul>
                </div>
                <div class="blockLineRed blockLineRed--margin blockLineRed--margin--wi">
                </div>

            </div>
            <div class="tab activeTab ">
                <?php if ($error_warning) { ?>
                    <div class="alert alert-danger"><i
                                class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
                <?php } ?>
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data"
                      class="formRegister" id="formRegister">
                    <div class="row">
                        <div class="col-md-4">
                            <p class="nameClient">Редактирование личных данных</p>

                            <input class="formRegister__inpForm" type="text" name="firstname"
                                   placeholder="Ваше имя *"
                                   value="<?php echo $firstname; ?>"
                                   required>
                            <?php if ($error_firstname) { ?>
                                <div class="text-danger"><?php echo $error_firstname; ?></div>
                            <?php } ?>

                            <input class="formRegister__inpForm" type="text" name="lastname"
                                   placeholder="Ваша фамилия *"
                                   value="<?php echo $lastname; ?>"
                                   required>
                            <?php if ($error_lastname) { ?>
                                <div class="text-danger"><?php echo $error_lastname; ?></div>
                            <?php } ?>

                            <input class="formRegister__inpForm" type="text" name="telephone" id="phone"
                                   placeholder="Телефон *"
                                   value="<?php echo $telephone; ?>"
                                   required>
                            <?php if ($error_telephone) { ?>
                                <div class="text-danger"><?php echo $error_telephone; ?></div>
                            <?php } ?>
                            <input class="formRegister__inpForm" type="email" name="email" placeholder="E-mail *"
                                   value="<?php echo $email; ?>"
                                   required>
                            <?php if ($error_email) { ?>
                                <div class="text-danger"><?php echo $error_email; ?></div>
                            <?php } ?>
                            <input class="formRegister__inpForm datepicker-here dateAge" type="text"
                                   name="birth_date" id="date"
                                   value="<?= $birth_date ?>"
                                   placeholder="Дата рождения">

                        </div>
                        <div class="col-md-4">
                            <p class="nameClient">Редактирование адреса доставки</p>

                            <div class="wrap-select">
                                <select name="country_id" id="input-country"
                                        class="formRegister__inpForm formRegister__inpForm--select">
                                    <option value=""><?php echo $text_select; ?></option>
                                    <?php foreach ($countries as $country) { ?>
                                        <?php if ($country['country_id'] == $country_id) { ?>
                                            <option value="<?php echo $country['country_id']; ?>"
                                                    selected="selected"><?php echo $country['name']; ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                                <?php if ($error_country) { ?>
                                    <div class="text-danger"><?php echo $error_country; ?></div>
                                <?php } ?>
                            </div>


                            <div class="wrap-select">
                                <select name="zone_id" id="input-zone"
                                        class="formRegister__inpForm formRegister__inpForm--select ">
                                </select>
                                <?php if ($error_zone) { ?>
                                    <div class="text-danger"><?php echo $error_zone; ?></div>
                                <?php } ?>
                            </div>

                            <input class="formRegister__inpForm" type="text" name="city" placeholder="Город *"
                                   value="<?php echo $city; ?>"
                                   required>
                            <?php if ($error_city) { ?>
                                <div class="text-danger"><?php echo $error_city; ?></div>
                            <?php } ?>

                            <input class="formRegister__inpForm" type="text" name="postcode" placeholder="Индекс"
                                   value="<?php echo $postcode; ?>"
                                   required>
                            <?php if ($error_postcode) { ?>
                                <div class="text-danger"><?php echo $error_postcode; ?></div>
                            <?php } ?>

                            <input class="formRegister__inpForm" type="text" name="address_1" placeholder="Адрес"
                                   value="<?php echo $address_1; ?>"
                                   required>
                            <?php if ($error_address_1) { ?>
                                <div class="text-danger"><?php echo $error_address_1; ?></div>
                            <?php } ?>
                            <input type="hidden" name="address_id" value="<?= $address_id ?>">


                        </div>
                        <div class="col-md-4">
                            <p class="nameClient">Изменение пароля</p>
                            <input class="formRegister__inpForm" type="password" name="password" id="input-password"
                                   value=""
                                   placeholder="Пароль *">
                            <?php if ($error_password) { ?>
                                <div class="text-danger"><?php echo $error_password; ?></div>
                            <?php } ?>

                            <input class="formRegister__inpForm" id="input-confirm" type="password" name="confirm"
                                   value=""
                                   placeholder="Повтор пароля *">
                            <?php if ($error_confirm) { ?>
                                <div class="text-danger"><?php echo $error_confirm; ?></div>
                            <?php } ?>

                            <div class="wrapPass">
                                <a href="#" class="rememPass">Забыли пароль ?</a>
                            </div>
                        </div>
                        <input class="formRegister__sub" type="submit" value="Сохранить">

                    </div>
                </form>

            </div>
            <div class="tab">
                <div class="wrapHideBaskedContent wrapHideBaskedContent--wi" id="wrapHideBaskedContent">
                    <?php if ($products) { ?>
                        <?php foreach ($products as $product) { ?>
                            <div class="wrapHideBaskedContent__item">
                                <div class="row">
                                    <div class="col-md-7 col-sm-7 col-xs-7">
                                        <div class="row">
                                            <div class="col-md-3 col-sm-3 col-xs-3">
                                                <div class="wrapHideBaskedContent__img">
                                                    <img src="<?php echo $product['thumb']; ?>"
                                                         alt="<?php echo $product['name']; ?>"
                                                         title="<?php echo $product['name']; ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-9 col-sm-9 col-xs-9">
                                                <div class="contentBasked">
                                                    <p class="contentBasked__tittle contentBasked__tittle--dis">
                                                        <?php echo $product['name']; ?> <span>Denis</span></p>
                                                    <?php if (!empty($product['rating']) || (int)$product['rating'] === 0) { ?>
                                                        <div class="star star--pad contentBasked__tittle--dis">
                                                            <?php for ($i = 1; $i <= 5; $i++) {
                                                                $ratingClass = '';

                                                                if ($product['rating'] < $i) {
                                                                    $ratingClass = 'star-star1';
                                                                } else {
                                                                    $ratingClass = 'star-star3';
                                                                } ?>
                                                                <div class="<?= $ratingClass ?>"></div>
                                                            <?php } ?>
                                                        </div>
                                                    <?php } ?>
                                                    <p class="contentBasked__text">Модель :
                                                        <span><?php echo $product['model']; ?></span>
                                                    </p>
                                                    <div class="wrapPriseSale marNo">
                                                        <?php if (!$product['special']) { ?>
                                                            <span class="wrapPriseSale__1"><?php echo $product['price']; ?></span>
                                                        <?php } else { ?>
                                                            <span class="wrapPriseSale__1"><?php echo $product['price']; ?></span>
                                                            <span class="wrapPriseSale__2"><?php echo $product['special']; ?></span>
                                                        <?php } ?>
                                                    </div>
                                                    <p class="<?= $product['stock_status']['class']?>"><?= $product['stock_status']['text']?></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-5 col-sm-5 col-xs-5">
                                        <div class="priceBasked">
                                            <span onclick="wishlist.remove('<?= $product['product_id']?>', this)" class="priceBasked__delete priceBasked__delete--pos2 priceBasked__delete--pos"></span>
                                            <div class="wrapButton clearfix">
                                                <button class="priceBasked__btn" onclick="cart.add('<?php echo $product['product_id']; ?>');">
                                                    ДОБАВИТЬ В КОРЗИНУ
                                                </button>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } else { ?>
                        <p><?php echo $text_empty; ?></p>
                    <?php } ?>
                </div>
            </div>
            <div class="tab">
                <div class="wrapTable">
                    <p class="wrapTable__tittle">Ваши заказы</p>
                    <?php if (!empty($orders)) { ?>
                        <select name="order_sort" id="order_sort" title="Сортировать заказы"
                                class="formRegister__inpForm formRegister__inpForm--select">
                            <option value="0">Все</option>
                            <option value="30">За месяц</option>
                            <option value="90">За квартал</option>
                        </select>
                    <?php } ?>
                </div>
                <div id="orders_info_container" class="wrapContentClient">
                    <?php if (!empty($orders)) { ?>
                        <?php foreach ($orders as $order) { ?>
                            <div class="wrapClientTable">
                                <div class="hedTable">

                                    <p class="zacazT">Заказ №
                                        <span><?php echo $order['order_id']; ?></span>, <?php echo $order['date_added']; ?>
                                    </p>
                                    <p class="<?= $order_status_id[$order['order_id']] == 10 ? 'zacazT--red' : 'zacazT--red zacazT--green' ?>"><?php echo $order['status']; ?> </p>

                                </div>
                                <table class="table">
                                    <?php
                                    $toPrintName = true;
                                    foreach ($order['products_info'] as $product) { ?>
                                        <tr>
                                            <td class="tName"><?= $toPrintName ? 'Название :' : '' ?></td>
                                            <td class="tItem"><?= $product['name'] ?></td>
                                            <td class="collT"><?= $product['quantity'] ?></td>
                                            <td class="lastTPr"><?= round($product['price']) ?> грн</td>
                                        </tr>
                                        <?php
                                        $toPrintName = false;
                                    } ?>

                                    <tr>
                                        <td class="tName">Адрес доставки :</td>
                                        <td class="tItem">
                                            тел. <?= $telephone ?> <?php echo $payment_address[$order['order_id']]; ?></td>
                                        <td class="collT collT--col"></td>
                                        <td class="lastTPr"></td>
                                    </tr>
                                    <?php foreach ($totals[$order['order_id']] as $total) { ?>
                                        <tr>
                                            <td class="tName"></td>
                                            <td class="tItem"></td>

                                            <td class="collT collT--col tName--col"><?php echo $total['title']; ?></td>
                                            <td class="lastTPr"><?php echo $total['text']; ?></td>
                                        </tr>
                                    <?php } ?>

                                </table>
                                <div class="tableFooter clearfix">
                                    <div class="tableFooter--wrapText">
                                        <p class="tableFooter--wrapText__left">итого</p>
                                        <p class="tableFooter--wrapText__right"><?php echo $order['total']; ?></p>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } else { ?>
                        <p>У вас нет заказов</p>
                    <?php } ?>
                </div>

            </div>
            <div class="tab">
                <div class="wrapClientTable">
                    <div class="hedTable">
                        <p class="zacazT">Бонусная система</p>
                    </div>

                    <table class="table">
                        <?php if ($rewards['rewards']) { ?>
                            <?php foreach ($rewards['rewards'] as $reward) { ?>
                                <tr>
                                    <td class="text-left"><?php echo $reward['date_added']; ?></td>
                                    <td class="text-left"><?php if ($reward['order_id']) { ?>
                                            <a href="<?php echo $reward['href']; ?>"><?php echo $reward['description']; ?></a>
                                        <?php } else { ?>
                                            <?php echo $reward['description']; ?>
                                        <?php } ?></td>
                                    <td class="text-right"><?php echo $reward['points']; ?></td>
                                </tr>
                            <?php } ?>
                        <?php } else { ?>
                            <tr>
                                <td colspan="3"><?php echo $rewards['text_empty']; ?></td>
                            </tr>
                        <?php } ?>
                    </table>

                </div>
                <p class="textLastZacaz">
                    Доставка товара по Киеву - бесплатно курьером. В другие города Украины - отправка товара по Новой
                    Почте наложенным платежем. При заказе от 1500 грн доставка будет бесплатной в любой регион. Также
                    принимайте участие в клиентской программе DENIS CLUB, чтобы получать скидки, подарки, дисконтную
                    карту.
                    При оформлении заказа Вы сможете воспользоваться Бонусной системой. Вы сэкономите свои средства и
                    получаете подарок от нас.
                </p>
            </div>
        </div>
    </div>
    </div>

</section>

<script type="text/javascript"><!--
    $('select[name=\'country_id\']').on('change', function () {
        $.ajax({
            url: 'index.php?route=account/account/country&country_id=' + this.value,
            dataType: 'json',
            beforeSend: function () {
                $('select[name=\'country_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
            },
            complete: function () {
                $('.fa-spin').remove();
            },
            success: function (json) {
                if (json['postcode_required'] == '1') {
                    $('input[name=\'postcode\']').parent().parent().addClass('required');
                } else {
                    $('input[name=\'postcode\']').parent().parent().removeClass('required');
                }

                html = '<option value=""><?php echo $text_select; ?></option>';

                if (json['zone'] && json['zone'] != '') {
                    for (i = 0; i < json['zone'].length; i++) {
                        html += '<option value="' + json['zone'][i]['zone_id'] + '"';

                        if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
                            html += ' selected="selected"';
                        }

                        html += '>' + json['zone'][i]['name'] + '</option>';
                    }
                } else {
                    html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
                }

                $('select[name=\'zone_id\']').html(html);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });

    $('select[name=\'country_id\']').trigger('change');
    //--></script>
<?php echo $footer; ?>
