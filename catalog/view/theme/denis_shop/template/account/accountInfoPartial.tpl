<div class="row">
    <div class="col-md-6 col-sm-6">
        <div class="wrapPersonTittle">
            <div class="nameFirst">
                <p class="nameFirst__left">Имя и Фамилия :</p>
                <p class="nameFirst__right"><?="$firstname $lastname"?></p>
            </div>
            <div class="nameFirst">
                <p class="nameFirst__left">E-mail :</p>
                <p class="nameFirst__right"><?=$email?></p>
            </div>
            <div class="nameFirst">
                <p class="nameFirst__left">Телефон</p>
                <p class="nameFirst__right"><?=$telephone?></p>
            </div>
            <div class="wrapChengNam">
                <a href="#" class="wrapChengNam__a" data-toggle="modal" data-target="#myModal2">Изменить
                    контактную информацию</a>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="wrapPersonTittle">
            <div class="nameFirst nameFirst--wi">
                <p class="nameFirst__left nameFirst__left--wi">Адрес доставки :</p>
                <p class="nameFirst__right">
                    <?= $address['city'] ?>, <?= $address['address_1']?>
                </p>
            </div>
            <div class="">
                <p class="nameFirst__left nameFirst__left--wi">Вариант доставки :</p>
                <?php if ($error_warning) { ?>
                    <div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
                <?php } ?>
                <?php if ($shipping_methods) { ?>
                    <p><?php echo $text_shipping_method; ?></p>
                    <?php foreach ($shipping_methods as $shipping_method) { ?>
                        <p><strong><?php echo $shipping_method['title']; ?></strong></p>
                        <?php if (!$shipping_method['error']) { ?>
                            <?php foreach ($shipping_method['quote'] as $quote) { ?>
                                    <label>
                                        <?php if ($quote['code'] == $code || !$code) { ?>
                                            <?php $code = $quote['code']; ?>
                                            <input type="radio" name="shipping_method" value="<?php echo $quote['code']; ?>"
                                                   checked="checked"/>
                                        <?php } else { ?>
                                            <input type="radio" name="shipping_method" value="<?php echo $quote['code']; ?>"/>
                                        <?php } ?>
                                        <?php echo $quote['title']; ?> - <?php echo $quote['text']; ?>
                                        <?php if (isset($quote['description'])) { ?>
                                            <br/>
                                            <small><?php echo $quote['description']; ?></small>
                                        <?php } ?>
                                    </label>
                            <?php } ?>
                        <?php } else { ?>
                            <div class="alert alert-danger"><?php echo $shipping_method['error']; ?></div>
                        <?php } ?>
                        <br>
                    <?php } ?>
                <?php } ?>
            </div>
            <div class="wrapChengNam">
                <a href="#" class="wrapChengNam__a wrapChengNam__a--noMar" data-toggle="modal"
                   data-target="#myModal3">Изменить адрес и вариант доставки</a>
            </div>
            <div class="wrapForm2 wrapForm2--pad">
                                    <textarea class="wrapForm2__comments"
                                              placeholder="Добавить комментарий к заказу"></textarea>
                <div class="wrapChec wrapChec--op">
                    <input class="formRegister__chec "
                           name="checkboxX"
                           type="checkbox" checked>
                    <label class="labelChec labelChec--clirnt "
                           for="checForm">Воспользоваться бонусами</label>
                    <div class="wrapTitle">
                        <span class="wrapTitle__qwestions">?</span>
                        <p class="podskazka">
                            Использовать бонусы могут только зарегистрированные
                            клиенты. Бонусы отображаются в вашем личном кабинете.
                            Детали использования бонусов, уточняются у менеджера при
                            оформлении заказа.
                        </p>
                    </div>
                </div>
            </div>
            <input class="formRegister__sub" type="button" value="ОФОРМИТЬ ЗАКАЗ" onclick="customOrderSubmit()">
        </div>
    </div>
</div>

<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modal-content2">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

            </div>

            <div class="modal-body">
                <div class="contMod2">
                    <div class="row">
                        <div id="info-result"></div>
                        <form action="#" id="form-account-info" class="modal1">
                            <div class="col-md-6 col-md-6--float">
                                <p class="nameClient nameClient--pad">Редактирование личных данных</p>
                                <input class="formRegister__inpForm" type="text" name="firstname" placeholder="Ваше имя *"
                                       value="<?= $firstname?>">
                                <div class="text-danger" id="error-firstname"></div>
                                <input class="formRegister__inpForm" type="text" name="lastname"
                                       placeholder="Ваша фамилия *"
                                       value="<?php echo $lastname; ?>"
                                >
                                <div class="text-danger" id="error-lastname"></div>
                                <input class="formRegister__inpForm" type="text" name="telephone" id="phone"
                                       placeholder="Телефон *"
                                       value="<?php echo $telephone; ?>"
                                >
                                <div class="text-danger" id="error-telephone"></div>
                                <input class="formRegister__inpForm" type="email" name="email" placeholder="E-mail *"
                                       value="<?php echo $email; ?>"
                                >
                                <div class="text-danger" id="error-email"></div>
                                <input class="formRegister__inpForm datepicker-here dateAge" type="text" name="birth_date"
                                       value="<?= $birth_date ?>"
                                       id="date"
                                       placeholder="Дата рождения">
                            </div>
                            <div class="col-md-6">
                                <p class="nameClient">Изменение пароля</p>
                                <input class="formRegister__inpForm" type="password" name="password" id="pass"
                                       placeholder="Пароль *">
                                <div class="text-danger" id="error-password"></div>
                                <input class="formRegister__inpForm" type="password" name="confirm"
                                       placeholder="Повтор пароля *">
                                <div class="text-danger" id="error-confirm"></div>
                                <input class="formRegister__sub" type="submit" value="ЗАРЕГИСТРИРОВАТЬСЯ">
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modal-content3">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <div class="contMod3">
                    <form action="#" class="formPop2">
                        <div class="wrapHei">
                            <p class="nameClient">Редактировать адрес и вариант доставки</p>

                            <select class="formRegister__inpForm formRegister__inpForm--select" name="deliveri"
                                    id="deliveri2">
                                <option value="1">Курьером (только Киев)</option>
                                <option value="2">Новая почта</option>
                            </select>
                            <div class="wrap-select">

                                <select class="formRegister__inpForm formRegister__inpForm--select formRegister__inpForm--none"
                                        name="posht" id="sity2">
                                    <option value="11">Выбрать город</option>
                                    <option value="1">Киев</option>
                                    <option value="2">Сумы</option>
                                </select>
                                <input class="formRegister__inpForm" id="street2" type="text" name="street"
                                       placeholder="Улица *">
                            </div>

                            <div class="wrapSelect2">
                                <input class="formRegister__inpForm formRegister__inpForm--home formRegister__inpForm--mar"
                                       type="text" name="home" id="home2" placeholder="Дом *">
                                <input class="formRegister__inpForm formRegister__inpForm--home " id="kvart2"
                                       type="text" name="kvart"
                                       placeholder="Квартира">
                                <input class="formRegister__inpForm" type="text" id="metro2" name="index"
                                       placeholder="Метро">

                                <select class="formRegister__inpForm formRegister__inpForm--select formRegister__inpForm--none"
                                        name="posht" id="poshtN2">
                                    <option value="11">Отделение Новой почты</option>
                                    <option value="1">N8</option>
                                    <option value="2">N7</option>
                                </select>
                            </div>
                        </div>
                        <input class="formRegister__sub formRegister__sub--mar" type="submit" value="Сохранить">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $('#form-account-info').submit(function (event) {
      event.preventDefault();

      $.ajax({
        type: 'POST',
        url: '/index.php?route=account/account/ajaxEditAccountInfo',
        data: $(event.target).serialize(),
        success: function (response) {
          var objResponse = JSON.parse(response);
          if (objResponse.result === 'failed') {
            if (objResponse.error_firstname) $('#error-firstname').text(objResponse.error_firstname);
            if (objResponse.error_lastname) $('#error-lastname').text(objResponse.error_lastname);
            if (objResponse.error_telephone) $('#error-telephone').text(objResponse.error_telephone);
            if (objResponse.error_email) $('#error-email').text(objResponse.error_email);
            if (objResponse.error_password) $('#error-password').text(objResponse.error_password);
            if (objResponse.error_confirm) $('#error-confirm').text(objResponse.error_confirm);
            $('#info-result').text('');

          } else {
            console.log($('#info-result'));
            $('#info-result').text('Информация обновлена');
          }
        }
      })
    })
</script>
