<?php echo $header; ?>
<section>
    <div class="container">
        <div class="SencsText">
            <h1 class="SencsText__tittle">Выход</h1>
            <p class="SencsText__text">
                Вы вышли из Вашего Личного Кабинета.
                Ваша корзина покупок была сохранена. Она будет восстановлена при следующем входе в Личный Кабинет.
            </p>
        </div>
    </div>
</section>

<?php echo $footer; ?>

