<?php if ($orders) { ?>
    <?php foreach ($orders as $order) { ?>
        <div class="wrapClientTable">
            <div class="hedTable">

                <p class="zacazT">Заказ № <span><?php echo $order['order_id']; ?></span>, <?php echo $order['date_added']; ?></p>
                <p class="<?= $order_status_id[$order['order_id']] == 10 ?  'zacazT--red' : 'zacazT--red zacazT--green'?>"><?php echo $order['status']; ?> </p>

            </div>
            <table class="table">
                <?php
                $toPrintName = true;
                foreach ($order['products_info'] as $product) {?>
                    <tr>
                        <td class="tName"><?= $toPrintName ? 'Название :' : ''?></td>
                        <td class="tItem"><?= $product['name']?></td>
                        <td class="collT"><?= $product['quantity']?></td>
                        <td class="lastTPr"><?= round($product['price'])?> грн</td>
                    </tr>
                    <?php
                    $toPrintName = false;
                } ?>

                <tr>
                    <td class="tName">Адрес доставки :</td>
                    <td class="tItem">тел. <?= $telephone ?> <?php echo $payment_address[$order['order_id']]; ?></td>
                    <td class="collT collT--col"></td>
                    <td class="lastTPr"></td>
                </tr>
                <?php foreach ($totals[$order['order_id']] as $total) { ?>
                    <tr>
                        <td class="tName"></td>
                        <td class="tItem"></td>

                        <td class="collT collT--col tName--col"><?php echo $total['title']; ?></td>
                        <td class="lastTPr"><?php echo $total['text']; ?></td>
                    </tr>
                <?php } ?>

            </table>
            <div class="tableFooter clearfix">
                <div class="tableFooter--wrapText">
                    <p class="tableFooter--wrapText__left">итого</p>
                    <p class="tableFooter--wrapText__right"><?php echo $order['total']; ?></p>
                </div>
            </div>
        </div>
    <?php } ?>
<?php }  else { ?>
    <p><?php echo $text_empty; ?></p>
<?php } ?>