<?php echo $header; ?>
    <section>
        <h1 class="tittleZacaz">
            оформление заказа
        </h1>
        <div class="zacazContainer">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 ">
                        <div class="wrapClient" id="wrapClient2">
                            <p class="nevClient2">
                                Личный данные
                            </p>
                            <p class="nevClient2">
                                Адрес доставки
                            </p>
                        </div>
                        <div class="wrapClient" id="wrapClient1">
                            <ul class="nav-tabs" id="myTab">
                            <?php if (!$logged) { ?>
                                <li class="blockTabsTabs__con activeTab">
                                    <a href="#newClient" class="nevClient">
                                        новый покупатель
                                    </a>
                                </li>
                                <li class="blockTabsTabs__con blockTabsTabs__con--color">
                                    <a href="#oldClient" class="oldClient">
                                        Я постоянный клиент
                                    </a>
                                </li>
                            </ul>
                            <?php }?>
                        </div>
                        <div class="formEnterOld1">
                            <div class="tab activeTab" id="newClient">
                                <div class="row">
                                    <form action="#" id="formClient1">
                                        <div class="col-md-7">
                                            <div class="wrForm1">
                                                <span class="wrForm1__span wrForm1__span--active one">
                                                    1
                                                </span>
                                                <p class="wrForm1__tittle">
                                                    Личный данные
                                                </p>
                                            </div>
                                            <div class="wrapPol">
                                                <input class="formRegister__inpForm" id="name" type="text" name="firstname"
                                                       placeholder="Ваше имя" required value="" oninput="validator.length(this, 3, 20)">
                                                <input class="formRegister__inpForm" id="firstName" type="text"
                                                       name="lastname" value="" oninput="validator.length(this, 3, 20)"
                                                       placeholder="Ваша фамилия" required>
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="wrForm1">
                                                <span class="wrForm1__span two">
                                                    2
                                                </span>
                                                <p class="wrForm1__tittle">
                                                    Адрес доставки
                                                </p>
                                            </div>
                                            <input class="formRegister__inpForm" id="phoneNumber" type="text" name="telephone"
                                                   placeholder="Телефон" required value="">
                                            <input class="formRegister__inpForm" id="email" type="email" name="email"
                                                   placeholder="E-mail" required value="">
                                            <input class="formRegister__sub formRegister__sub--next " id="oform2"
                                                   type="submit" value="Далее">
                                        </div>
                                    </form>
                                    <form action="#" id="formClient">
                                        <div class="col-md-7">
                                            <div class="wrForm1">
                                                <span class="wrForm1__span wrForm1__span--active one">
                                                    1
                                                </span>
                                                <p class="wrForm1__tittle">
                                                    Личный данные
                                                </p>
                                            </div>
                                            <div class="wrapPol">
                                                <select class="formRegister__inpForm formRegister__inpForm--select"
                                                        name="zone_id" id="zone_id">
                                                </select>
                                                <div class="wrapSelect2">
                                                    <input class="formRegister__inpForm formRegister__inpForm--home formRegister__inpForm--mar"
                                                           type="text" name="city" id="city" placeholder="Город"
                                                           value="" oninput="validator.length(this, 3, 20)"
                                                           required>
                                                    <input class="formRegister__inpForm formRegister__inpForm--home formRegister__inpForm--mar"
                                                           type="text" name="postcode" id="home" placeholder="Индекс"
                                                           value="" oninput="validator.numbers(this)"
                                                           required>
                                                    <input class="formRegister__inpForm formRegister__inpForm--home formRegister__inpForm--mar"
                                                           type="text" name="address_1" id="home" placeholder="Адресс"
                                                           value="Lenina 1"
                                                           required>
                                                    <input type="hidden" name="country_id" value="220">
                                                </div>

                                                <p class="nameFirst__left nameFirst__left--wi">Вариант доставки :</p>
                                                <?php if ($error_warning) { ?>
                                                    <div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
                                                <?php } ?>
                                                <?php if ($shipping_methods) { ?>
                                                    <p><?php echo $text_shipping_method; ?></p>
                                                    <?php foreach ($shipping_methods as $shipping_method) { ?>
                                                        <p><strong><?php echo $shipping_method['title']; ?></strong></p>
                                                        <?php if (!$shipping_method['error']) { ?>
                                                            <?php foreach ($shipping_method['quote'] as $quote) { ?>
                                                                <label>
                                                                    <?php if ($quote['code'] == $code || !$code) { ?>
                                                                        <?php $code = $quote['code']; ?>
                                                                        <input type="radio" name="shipping_method" value="<?php echo $quote['code']; ?>"
                                                                               checked="checked"/>
                                                                    <?php } else { ?>
                                                                        <input type="radio" name="shipping_method" value="<?php echo $quote['code']; ?>"/>
                                                                    <?php } ?>
                                                                    <?php echo $quote['title']; ?> - <?php echo $quote['text']; ?>
                                                                    <?php if (isset($quote['description'])) { ?>
                                                                        <br/>
                                                                        <small><?php echo $quote['description']; ?></small>
                                                                    <?php } ?>
                                                                </label>
                                                            <?php } ?>
                                                        <?php } else { ?>
                                                            <div class="alert alert-danger"><?php echo $shipping_method['error']; ?></div>
                                                        <?php } ?>
                                                        <br>
                                                    <?php } ?>
                                                <?php } ?>


                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="wrForm1">
                                                <span class="wrForm1__span two">2</span>
                                                <p class="wrForm1__tittle">Адрес доставки</p>
                                            </div>
                                            <div class="wrapForm2">
                                                <textarea class="wrapForm2__comments"
                                                    placeholder="Добавить комментарий к заказу" name="comment"></textarea>

                                            </div>
                                            <input class="formRegister__sub" type="button" id="order-form"
                                                   value="ОФОРМИТЬ ЗАКАЗ">
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="tab" id="oldClient">

                                <p class="formOldTittle">
                                    Если вы зарегистрированный клиент введите ваш логин/email и пароль, если вы еще не
                                    являетесь
                                    нашим клиентом, то перейдите в раздел "Новый покупатель" и заполните информацию.
                                </p>
                                <div class="row">
                                    <form action="#" class="formEnter" id="formEnter">
                                        <div class="col-md-6">
                                            <input class="formRegister__inpForm formRegister__inpForm--color"
                                                   id="emailEnter" type="email" name="email"
                                                   placeholder="Введите Ваш email" required>
                                        </div>
                                        <div class="col-md-6">
                                            <input class="formRegister__inpForm formRegister__inpForm--color" id="passEnter"
                                                   type="password" name="password"
                                                   placeholder="Введите Ваш пароль" required>
                                            <input class="formRegister__sub formRegister__sub--enter " id="oformEnter"
                                                   type="submit"
                                                   value="ВОЙТИ">
                                            <a href="#"
                                               class="wrapBottomFormRegister__rememberPass wrapBottomFormRegister__rememberPass--enter">Забыли
                                                пароль ?</a>
                                        </div>
                                    </form>

                                </div>


                            </div>
                        </div>
                        <div class="formEnterOld2" id="account-info"></div>

                    </div>




                    <div class="col-md-4 borderR" id="checkout-cart">


                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>










    <div class="container">
        <ul class="breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
        </ul>
        <?php if ($error_warning) { ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>
        <div class="row"><?php echo $column_left; ?>
            <?php if ($column_left && $column_right) { ?>
                <?php $class = 'col-sm-6'; ?>
            <?php } elseif ($column_left || $column_right) { ?>
                <?php $class = 'col-sm-9'; ?>
            <?php } else { ?>
                <?php $class = 'col-sm-12'; ?>
            <?php } ?>
            <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
                <h1><?php echo $heading_title; ?></h1>
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title"><?php echo $text_checkout_option; ?></h4>
                        </div>
                        <div class="panel-collapse collapse" id="collapse-checkout-option">
                            <div class="panel-body"></div>
                        </div>
                    </div>
                    <?php if (!$logged && $account != 'guest') { ?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title"><?php echo $text_checkout_account; ?></h4>
                            </div>
                            <div class="panel-collapse collapse" id="collapse-payment-address">
                                <div class="panel-body"></div>
                            </div>
                        </div>
                    <?php } else { ?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title"><?php echo $text_checkout_payment_address; ?></h4>
                            </div>
                            <div class="panel-collapse collapse" id="collapse-payment-address">
                                <div class="panel-body"></div>
                            </div>
                        </div>
                    <?php } ?>
                    <?php if ($shipping_required) { ?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title"><?php echo $text_checkout_shipping_address; ?></h4>
                            </div>
                            <div class="panel-collapse collapse" id="collapse-shipping-address">
                                <div class="panel-body"></div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title"><?php echo $text_checkout_shipping_method; ?></h4>
                            </div>
                            <div class="panel-collapse collapse" id="collapse-shipping-method">
                                <div class="panel-body"></div>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title"><?php echo $text_checkout_payment_method; ?></h4>
                        </div>
                        <div class="panel-collapse collapse" id="collapse-payment-method">
                            <div class="panel-body"></div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title"><?php echo $text_checkout_confirm; ?></h4>
                        </div>
                        <div class="panel-collapse collapse" id="collapse-checkout-confirm">
                            <div class="panel-body"></div>
                        </div>
                    </div>
                </div>
                <?php echo $content_bottom; ?></div>
            <?php echo $column_right; ?></div>
    </div>
    <script type="text/javascript"><!--
        $(document).on('change', 'input[name=\'account\']', function () {
            if ($('#collapse-payment-address').parent().find('.panel-heading .panel-title > *').is('a')) {
                if (this.value == 'register') {
                    $('#collapse-payment-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-payment-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_account; ?> <i class="fa fa-caret-down"></i></a>');
                } else {
                    $('#collapse-payment-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-payment-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_payment_address; ?> <i class="fa fa-caret-down"></i></a>');
                }
            } else {
                if (this.value == 'register') {
                    $('#collapse-payment-address').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_account; ?>');
                } else {
                    $('#collapse-payment-address').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_payment_address; ?>');
                }
            }
        });

        <?php if (!$logged) { ?>
        $(document).ready(function () {
            $.ajax({
                url: 'index.php?route=checkout/login',
                dataType: 'html',
                success: function (html) {
                    $('#collapse-checkout-option .panel-body').html(html);

                    $('#collapse-checkout-option').parent().find('.panel-heading .panel-title').html('<a href="#collapse-checkout-option" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_option; ?> <i class="fa fa-caret-down"></i></a>');

                    $('a[href=\'#collapse-checkout-option\']').trigger('click');
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        });
        <?php } else { ?>
        $(document).ready(function () {
            $.ajax({
                url: 'index.php?route=checkout/payment_address',
                dataType: 'html',
                success: function (html) {
                    $('#collapse-payment-address .panel-body').html(html);

                    $('#collapse-payment-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-payment-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_payment_address; ?> <i class="fa fa-caret-down"></i></a>');

                    $('a[href=\'#collapse-payment-address\']').trigger('click');
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        });
        <?php } ?>

        // Checkout
        $(document).delegate('#button-account', 'click', function () {
            $.ajax({
                url: 'index.php?route=checkout/' + $('input[name=\'account\']:checked').val(),
                dataType: 'html',
                beforeSend: function () {
                    $('#button-account').button('loading');
                },
                complete: function () {
                    $('#button-account').button('reset');
                },
                success: function (html) {
                    $('.alert, .text-danger').remove();

                    $('#collapse-payment-address .panel-body').html(html);

                    if ($('input[name=\'account\']:checked').val() == 'register') {
                        $('#collapse-payment-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-payment-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_account; ?> <i class="fa fa-caret-down"></i></a>');
                    } else {
                        $('#collapse-payment-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-payment-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_payment_address; ?> <i class="fa fa-caret-down"></i></a>');
                    }

                    $('a[href=\'#collapse-payment-address\']').trigger('click');
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        });

        // Login
        $(document).delegate('#button-login', 'click', function () {
            $.ajax({
                url: 'index.php?route=checkout/login/save',
                type: 'post',
                data: $('#collapse-checkout-option :input'),
                dataType: 'json',
                beforeSend: function () {
                    $('#button-login').button('loading');
                },
                complete: function () {
                    $('#button-login').button('reset');
                },
                success: function (json) {
                    $('.alert, .text-danger').remove();
                    $('.form-group').removeClass('has-error');

                    if (json['redirect']) {
                        location = json['redirect'];
                    } else if (json['error']) {
                        $('#collapse-checkout-option .panel-body').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                        // Highlight any found errors
                        $('input[name=\'email\']').parent().addClass('has-error');
                        $('input[name=\'password\']').parent().addClass('has-error');
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        });

        // Register
        $(document).delegate('#button-register', 'click', function () {
            $.ajax({
                url: 'index.php?route=checkout/register/save',
                type: 'post',
                data: $('#collapse-payment-address input[type=\'text\'], #collapse-payment-address input[type=\'date\'], #collapse-payment-address input[type=\'datetime-local\'], #collapse-payment-address input[type=\'time\'], #collapse-payment-address input[type=\'password\'], #collapse-payment-address input[type=\'hidden\'], #collapse-payment-address input[type=\'checkbox\']:checked, #collapse-payment-address input[type=\'radio\']:checked, #collapse-payment-address textarea, #collapse-payment-address select'),
                dataType: 'json',
                beforeSend: function () {
                    $('#button-register').button('loading');
                },
                success: function (json) {
                    $('.alert, .text-danger').remove();
                    $('.form-group').removeClass('has-error');

                    if (json['redirect']) {
                        location = json['redirect'];
                    } else if (json['error']) {
                        $('#button-register').button('reset');

                        if (json['error']['warning']) {
                            $('#collapse-payment-address .panel-body').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                        }

                        for (i in json['error']) {
                            var element = $('#input-payment-' + i.replace('_', '-'));

                            if ($(element).parent().hasClass('input-group')) {
                                $(element).parent().after('<div class="text-danger">' + json['error'][i] + '</div>');
                            } else {
                                $(element).after('<div class="text-danger">' + json['error'][i] + '</div>');
                            }
                        }

                        // Highlight any found errors
                        $('.text-danger').parent().addClass('has-error');
                    } else {
                        <?php if ($shipping_required) { ?>
                        var shipping_address = $('#payment-address input[name=\'shipping_address\']:checked').prop('value');

                        if (shipping_address) {
                            $.ajax({
                                url: 'index.php?route=checkout/shipping_method',
                                dataType: 'html',
                                success: function (html) {
                                    // Add the shipping address
                                    $.ajax({
                                        url: 'index.php?route=checkout/shipping_address',
                                        dataType: 'html',
                                        success: function (html) {
                                            $('#collapse-shipping-address .panel-body').html(html);

                                            $('#collapse-shipping-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_shipping_address; ?> <i class="fa fa-caret-down"></i></a>');
                                        },
                                        error: function (xhr, ajaxOptions, thrownError) {
                                            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                                        }
                                    });

                                    $('#collapse-shipping-method .panel-body').html(html);

                                    $('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_shipping_method; ?> <i class="fa fa-caret-down"></i></a>');

                                    $('a[href=\'#collapse-shipping-method\']').trigger('click');

                                    $('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_shipping_method; ?>');
                                    $('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_payment_method; ?>');
                                    $('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_confirm; ?>');
                                },
                                error: function (xhr, ajaxOptions, thrownError) {
                                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                                }
                            });
                        } else {
                            $.ajax({
                                url: 'index.php?route=checkout/shipping_address',
                                dataType: 'html',
                                success: function (html) {
                                    $('#collapse-shipping-address .panel-body').html(html);

                                    $('#collapse-shipping-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_shipping_address; ?> <i class="fa fa-caret-down"></i></a>');

                                    $('a[href=\'#collapse-shipping-address\']').trigger('click');

                                    $('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_shipping_method; ?>');
                                    $('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_payment_method; ?>');
                                    $('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_confirm; ?>');
                                },
                                error: function (xhr, ajaxOptions, thrownError) {
                                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                                }
                            });
                        }
                        <?php } else { ?>
                        $.ajax({
                            url: 'index.php?route=checkout/payment_method',
                            dataType: 'html',
                            success: function (html) {
                                $('#collapse-payment-method .panel-body').html(html);

                                $('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<a href="#collapse-payment-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_payment_method; ?> <i class="fa fa-caret-down"></i></a>');

                                $('a[href=\'#collapse-payment-method\']').trigger('click');

                                $('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_confirm; ?>');
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });
                        <?php } ?>

                        $.ajax({
                            url: 'index.php?route=checkout/payment_address',
                            dataType: 'html',
                            complete: function () {
                                $('#button-register').button('reset');
                            },
                            success: function (html) {
                                $('#collapse-payment-address .panel-body').html(html);

                                $('#collapse-payment-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-payment-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_payment_address; ?> <i class="fa fa-caret-down"></i></a>');
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        });

        // Payment Address
        $(document).delegate('#button-payment-address', 'click', function () {
            $.ajax({
                url: 'index.php?route=checkout/payment_address/save',
                type: 'post',
                data: $('#collapse-payment-address input[type=\'text\'], #collapse-payment-address input[type=\'date\'], #collapse-payment-address input[type=\'datetime-local\'], #collapse-payment-address input[type=\'time\'], #collapse-payment-address input[type=\'password\'], #collapse-payment-address input[type=\'checkbox\']:checked, #collapse-payment-address input[type=\'radio\']:checked, #collapse-payment-address input[type=\'hidden\'], #collapse-payment-address textarea, #collapse-payment-address select'),
                dataType: 'json',
                beforeSend: function () {
                    $('#button-payment-address').button('loading');
                },
                complete: function () {
                    $('#button-payment-address').button('reset');
                },
                success: function (json) {
                    $('.alert, .text-danger').remove();

                    if (json['redirect']) {
                        location = json['redirect'];
                    } else if (json['error']) {
                        if (json['error']['warning']) {
                            $('#collapse-payment-address .panel-body').prepend('<div class="alert alert-warning">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                        }

                        for (i in json['error']) {
                            var element = $('#input-payment-' + i.replace('_', '-'));

                            if ($(element).parent().hasClass('input-group')) {
                                $(element).parent().after('<div class="text-danger">' + json['error'][i] + '</div>');
                            } else {
                                $(element).after('<div class="text-danger">' + json['error'][i] + '</div>');
                            }
                        }

                        // Highlight any found errors
                        $('.text-danger').parent().parent().addClass('has-error');
                    } else {
                        <?php if ($shipping_required) { ?>
                        $.ajax({
                            url: 'index.php?route=checkout/shipping_address',
                            dataType: 'html',
                            success: function (html) {
                                $('#collapse-shipping-address .panel-body').html(html);

                                $('#collapse-shipping-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_shipping_address; ?> <i class="fa fa-caret-down"></i></a>');

                                $('a[href=\'#collapse-shipping-address\']').trigger('click');

                                $('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_shipping_method; ?>');
                                $('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_payment_method; ?>');
                                $('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_confirm; ?>');
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });
                        <?php } else { ?>
                        $.ajax({
                            url: 'index.php?route=checkout/payment_method',
                            dataType: 'html',
                            success: function (html) {
                                $('#collapse-payment-method .panel-body').html(html);

                                $('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<a href="#collapse-payment-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_payment_method; ?> <i class="fa fa-caret-down"></i></a>');

                                $('a[href=\'#collapse-payment-method\']').trigger('click');

                                $('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_confirm; ?>');
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });
                        <?php } ?>

                        $.ajax({
                            url: 'index.php?route=checkout/payment_address',
                            dataType: 'html',
                            success: function (html) {
                                $('#collapse-payment-address .panel-body').html(html);
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        });

        // Shipping Address
        $(document).delegate('#button-shipping-address', 'click', function () {
            $.ajax({
                url: 'index.php?route=checkout/shipping_address/save',
                type: 'post',
                data: $('#collapse-shipping-address input[type=\'text\'], #collapse-shipping-address input[type=\'date\'], #collapse-shipping-address input[type=\'datetime-local\'], #collapse-shipping-address input[type=\'time\'], #collapse-shipping-address input[type=\'password\'], #collapse-shipping-address input[type=\'checkbox\']:checked, #collapse-shipping-address input[type=\'radio\']:checked, #collapse-shipping-address textarea, #collapse-shipping-address select'),
                dataType: 'json',
                beforeSend: function () {
                    $('#button-shipping-address').button('loading');
                },
                success: function (json) {
                    $('.alert, .text-danger').remove();

                    if (json['redirect']) {
                        location = json['redirect'];
                    } else if (json['error']) {
                        $('#button-shipping-address').button('reset');

                        if (json['error']['warning']) {
                            $('#collapse-shipping-address .panel-body').prepend('<div class="alert alert-warning">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                        }

                        for (i in json['error']) {
                            var element = $('#input-shipping-' + i.replace('_', '-'));

                            if ($(element).parent().hasClass('input-group')) {
                                $(element).parent().after('<div class="text-danger">' + json['error'][i] + '</div>');
                            } else {
                                $(element).after('<div class="text-danger">' + json['error'][i] + '</div>');
                            }
                        }

                        // Highlight any found errors
                        $('.text-danger').parent().parent().addClass('has-error');
                    } else {
                        $.ajax({
                            url: 'index.php?route=checkout/shipping_method',
                            dataType: 'html',
                            complete: function () {
                                $('#button-shipping-address').button('reset');
                            },
                            success: function (html) {
                                $('#collapse-shipping-method .panel-body').html(html);

                                $('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_shipping_method; ?> <i class="fa fa-caret-down"></i></a>');

                                $('a[href=\'#collapse-shipping-method\']').trigger('click');

                                $('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_payment_method; ?>');
                                $('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_confirm; ?>');

                                $.ajax({
                                    url: 'index.php?route=checkout/shipping_address',
                                    dataType: 'html',
                                    success: function (html) {
                                        $('#collapse-shipping-address .panel-body').html(html);
                                    },
                                    error: function (xhr, ajaxOptions, thrownError) {
                                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                                    }
                                });
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });

                        $.ajax({
                            url: 'index.php?route=checkout/payment_address',
                            dataType: 'html',
                            success: function (html) {
                                $('#collapse-payment-address .panel-body').html(html);
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        });

        // Guest
        $(document).delegate('#button-guest', 'click', function () {
            $.ajax({
                url: 'index.php?route=checkout/guest/save',
                type: 'post',
                data: $('#collapse-payment-address input[type=\'text\'], #collapse-payment-address input[type=\'date\'], #collapse-payment-address input[type=\'datetime-local\'], #collapse-payment-address input[type=\'time\'], #collapse-payment-address input[type=\'checkbox\']:checked, #collapse-payment-address input[type=\'radio\']:checked, #collapse-payment-address input[type=\'hidden\'], #collapse-payment-address textarea, #collapse-payment-address select'),
                dataType: 'json',
                beforeSend: function () {
                    $('#button-guest').button('loading');
                },
                success: function (json) {
                    $('.alert, .text-danger').remove();

                    if (json['redirect']) {
                        location = json['redirect'];
                    } else if (json['error']) {
                        $('#button-guest').button('reset');

                        if (json['error']['warning']) {
                            $('#collapse-payment-address .panel-body').prepend('<div class="alert alert-warning">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                        }

                        for (i in json['error']) {
                            var element = $('#input-payment-' + i.replace('_', '-'));

                            if ($(element).parent().hasClass('input-group')) {
                                $(element).parent().after('<div class="text-danger">' + json['error'][i] + '</div>');
                            } else {
                                $(element).after('<div class="text-danger">' + json['error'][i] + '</div>');
                            }
                        }

                        // Highlight any found errors
                        $('.text-danger').parent().addClass('has-error');
                    } else {
                        <?php if ($shipping_required) { ?>
                        var shipping_address = $('#collapse-payment-address input[name=\'shipping_address\']:checked').prop('value');

                        if (shipping_address) {
                            $.ajax({
                                url: 'index.php?route=checkout/shipping_method',
                                dataType: 'html',
                                complete: function () {
                                    $('#button-guest').button('reset');
                                },
                                success: function (html) {
                                    // Add the shipping address
                                    $.ajax({
                                        url: 'index.php?route=checkout/guest_shipping',
                                        dataType: 'html',
                                        success: function (html) {
                                            $('#collapse-shipping-address .panel-body').html(html);

                                            $('#collapse-shipping-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_shipping_address; ?> <i class="fa fa-caret-down"></i></a>');
                                        },
                                        error: function (xhr, ajaxOptions, thrownError) {
                                            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                                        }
                                    });

                                    $('#collapse-shipping-method .panel-body').html(html);

                                    $('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_shipping_method; ?> <i class="fa fa-caret-down"></i></a>');

                                    $('a[href=\'#collapse-shipping-method\']').trigger('click');

                                    $('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_payment_method; ?>');
                                    $('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_confirm; ?>');
                                },
                                error: function (xhr, ajaxOptions, thrownError) {
                                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                                }
                            });
                        } else {
                            $.ajax({
                                url: 'index.php?route=checkout/guest_shipping',
                                dataType: 'html',
                                complete: function () {
                                    $('#button-guest').button('reset');
                                },
                                success: function (html) {
                                    $('#collapse-shipping-address .panel-body').html(html);

                                    $('#collapse-shipping-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_shipping_address; ?> <i class="fa fa-caret-down"></i></a>');

                                    $('a[href=\'#collapse-shipping-address\']').trigger('click');

                                    $('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_shipping_method; ?>');
                                    $('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_payment_method; ?>');
                                    $('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_confirm; ?>');
                                },
                                error: function (xhr, ajaxOptions, thrownError) {
                                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                                }
                            });
                        }
                        <?php } else { ?>
                        $.ajax({
                            url: 'index.php?route=checkout/payment_method',
                            dataType: 'html',
                            complete: function () {
                                $('#button-guest').button('reset');
                            },
                            success: function (html) {
                                $('#collapse-payment-method .panel-body').html(html);

                                $('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<a href="#collapse-payment-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_payment_method; ?> <i class="fa fa-caret-down"></i></a>');

                                $('a[href=\'#collapse-payment-method\']').trigger('click');

                                $('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_confirm; ?>');
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });
                        <?php } ?>
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        });

        // Guest Shipping
        $(document).delegate('#button-guest-shipping', 'click', function () {
            $.ajax({
                url: 'index.php?route=checkout/guest_shipping/save',
                type: 'post',
                data: $('#collapse-shipping-address input[type=\'text\'], #collapse-shipping-address input[type=\'date\'], #collapse-shipping-address input[type=\'datetime-local\'], #collapse-shipping-address input[type=\'time\'], #collapse-shipping-address input[type=\'password\'], #collapse-shipping-address input[type=\'checkbox\']:checked, #collapse-shipping-address input[type=\'radio\']:checked, #collapse-shipping-address textarea, #collapse-shipping-address select'),
                dataType: 'json',
                beforeSend: function () {
                    $('#button-guest-shipping').button('loading');
                },
                success: function (json) {
                    $('.alert, .text-danger').remove();

                    if (json['redirect']) {
                        location = json['redirect'];
                    } else if (json['error']) {
                        $('#button-guest-shipping').button('reset');

                        if (json['error']['warning']) {
                            $('#collapse-shipping-address .panel-body').prepend('<div class="alert alert-warning">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                        }

                        for (i in json['error']) {
                            var element = $('#input-shipping-' + i.replace('_', '-'));

                            if ($(element).parent().hasClass('input-group')) {
                                $(element).parent().after('<div class="text-danger">' + json['error'][i] + '</div>');
                            } else {
                                $(element).after('<div class="text-danger">' + json['error'][i] + '</div>');
                            }
                        }

                        // Highlight any found errors
                        $('.text-danger').parent().addClass('has-error');
                    } else {
                        $.ajax({
                            url: 'index.php?route=checkout/shipping_method',
                            dataType: 'html',
                            complete: function () {
                                $('#button-guest-shipping').button('reset');
                            },
                            success: function (html) {
                                $('#collapse-shipping-method .panel-body').html(html);

                                $('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_shipping_method; ?> <i class="fa fa-caret-down"></i>');

                                $('a[href=\'#collapse-shipping-method\']').trigger('click');

                                $('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_payment_method; ?>');
                                $('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_confirm; ?>');
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        });

        $(document).delegate('#button-shipping-method', 'click', function () {
            $.ajax({
                url: 'index.php?route=checkout/shipping_method/save',
                type: 'post',
                data: $('#collapse-shipping-method input[type=\'radio\']:checked, #collapse-shipping-method textarea'),
                dataType: 'json',
                beforeSend: function () {
                    $('#button-shipping-method').button('loading');
                },
                success: function (json) {
                    $('.alert, .text-danger').remove();

                    if (json['redirect']) {
                        location = json['redirect'];
                    } else if (json['error']) {
                        $('#button-shipping-method').button('reset');

                        if (json['error']['warning']) {
                            $('#collapse-shipping-method .panel-body').prepend('<div class="alert alert-warning">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                        }
                    } else {
                        $.ajax({
                            url: 'index.php?route=checkout/payment_method',
                            dataType: 'html',
                            complete: function () {
                                $('#button-shipping-method').button('reset');
                            },
                            success: function (html) {
                                $('#collapse-payment-method .panel-body').html(html);

                                $('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<a href="#collapse-payment-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_payment_method; ?> <i class="fa fa-caret-down"></i></a>');

                                $('a[href=\'#collapse-payment-method\']').trigger('click');

                                $('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_confirm; ?>');
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        });

        $(document).delegate('#button-payment-method', 'click', function () {
            $.ajax({
                url: 'index.php?route=checkout/payment_method/save',
                type: 'post',
                data: $('#collapse-payment-method input[type=\'radio\']:checked, #collapse-payment-method input[type=\'checkbox\']:checked, #collapse-payment-method textarea'),
                dataType: 'json',
                beforeSend: function () {
                    $('#button-payment-method').button('loading');
                },
                success: function (json) {
                    $('.alert, .text-danger').remove();

                    if (json['redirect']) {
                        location = json['redirect'];
                    } else if (json['error']) {
                        $('#button-payment-method').button('reset');
                        if (json['error']['warning']) {
                            $('#collapse-payment-method .panel-body').prepend('<div class="alert alert-warning">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                        }
                    } else {
                        $.ajax({
                            url: 'index.php?route=checkout/confirm',
                            dataType: 'html',
                            complete: function () {
                                $('#button-payment-method').button('reset');
                            },
                            success: function (html) {
                                $('#collapse-checkout-confirm .panel-body').html(html);

                                $('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('<a href="#collapse-checkout-confirm" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_confirm; ?> <i class="fa fa-caret-down"></i></a>');

                                $('a[href=\'#collapse-checkout-confirm\']').trigger('click');
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        });
        //--></script>
    <script>
      $('#checkout-cart').load('/index.php?route=checkout/confirm/customConfirm');
      <?php if($logged) {?>
        $('#account-info').load('/index.php?route=account/account/ajaxAccountInfo');
        $('.formEnterOld1').hide('slow');
        $('.formEnterOld2').show('slow');

        $('#wrapClient1').hide('slow');
        $('#wrapClient2').show('slow').css("display", "flex");
      <?php }?>

      $(document).ready(function () {
        $.ajax({
          url: 'index.php?route=checkout/checkout/country&country_id=220',
          dataType: 'json',
          success: function (json) {
            html = '<option value="">Выберите регион</option>';

            if (json['zone'] && json['zone'] != '') {
              for (i = 0; i < json['zone'].length; i++) {
                html += '<option value="' + json['zone'][i]['zone_id'] + '"';

                html += '>' + json['zone'][i]['name'] + '</option>';
              }
            } else {
              html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
            }

            $('#zone_id').html(html);
          },
          error: function (xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
          }
        });
      });
    </script>
<?php echo $footer; ?>