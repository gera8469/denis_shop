<div class="wrapClient wrapClient--mar">
    <a href="#" class="nevClient1">Ваш заказ</a>
    <a href="#" class="oldClient1">Продолжить покупки</a>
</div>
<!--<div class="wrapHideContent">-->
    <?php
    $toShowHR = true;
    foreach ($products as $product) { ?>
        <div class="wrapHideBaskedContent__item <?= $toShowHR ? 'wrapHideBaskedContent__item--bor' : ''?>">
            <div class="wrapHideBaskedContent__img wrapHideBaskedContent__img--wi ">
                <img src="<?= $product['thumb'] ?>" alt="">
            </div>
            <div class="contentBasked">
                <p class="contentBasked__tittle"><?php echo $product['name']; ?>
                    <span>Denis</span></p>
                <p class="contentBasked__text">Модель : <span><?php echo $product['model']; ?></span>
                </p>
                <?php foreach ($product['option'] as $option) { ?>
                    <p class="contentBasked__text"><?php echo $option['name']; ?> :
                        <span><?php echo $option['value']; ?></span></p>
                <?php } ?>
                <div class="wrapBtn wrapBtn--Basced">
                    <button class="wrapBtn__left" onclick="cart.updateCheckout(<?= $product['cart_id']?>, <?php echo $product['quantity'] - 1; ?>)">-</button>
                    <input type="text" class="wrapBtn__texInp"
                           placeholder="1" value="<?php echo $product['quantity']; ?>">
                    <button class="wrapBtn__right" onclick="cart.updateCheckout(<?= $product['cart_id']?>, <?php echo $product['quantity'] + 1; ?>)">+</button>
                    <span class="priceBasked__delete priceBasked__delete--pos" onclick="cart.checkoutRemove('<?php echo $product['cart_id'];  ?>');"></span>
                </div>
                <p class="priceBasked__text priceBasked__text--client"><?php echo $product['price']; ?></p>
            </div>
        </div>
    <?php
    $toShowHR = false;
    } ?>
<!--</div>-->
<?php foreach ($totals as $total) { ?>
    <div class="wrapPriceClient1">
        <p class="wrapPriceClient1__left"><?php echo $total['title']; ?></p>
        <p class="wrapPriceClient1__right"><?php echo $total['text']; ?></p>
    </div>
<?php } ?>
