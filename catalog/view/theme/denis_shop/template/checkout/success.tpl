<?php echo $header; ?>
    <section>
        <div class="container">
            <div class="SencsText">
                <h1 class="SencsText__tittle">Спасибо за ваш заказ</h1>
                <p class="SencsText__text">
                    В ближайшее время наш менеджер свяжется с Вами для уточнения деталий заказа
                </p>
                <?php if (!empty($order_id)) {?>
                    <p class="SencsText__number">Номер вашего заказа: <?= $order_id?></p>
                <?php } ?>

            </div>
        </div>
    </section>

<?php echo $footer; ?>