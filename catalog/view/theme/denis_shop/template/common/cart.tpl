<a id="cart" href="#" class="topHedRight__basked" data-toggle="modal" data-target="#Cart">
    <i class="clikMenu__circle"><?php echo $total_products; ?></i>
</a>
<div class="modal fade" id="Cart" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content modal-content--basked">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-hidden="true">&times;
                </button>

            </div>

            <div id="cart_content" class="modal-body">
                <p class="tittleBasket">КОрзина</p>
                <?php if ($products || $vouchers) { ?>
                    <div class="wrapHideBaskedContent">
                        <?php foreach ($products as $product) { ?>

                            <div class="wrapHideBaskedContent__item">
                                <div class="row">
                                    <div class="col-md-7 col-sm-7 col-xs-7">
                                        <div class="row">
                                            <div class="col-md-5 col-sm-5 col-xs-5">
                                                <div class="wrapHideBaskedContent__img">
                                                    <?php if ($product['thumb']) { ?>
                                                        <a href="<?php echo $product['href']; ?>">
                                                            <img src="<?php echo $product['thumb']; ?>"
                                                                 alt="<?php echo $product['name']; ?>"
                                                                 title="<?php echo $product['name']; ?>" class=""/>
                                                        </a>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <div class="col-md-7 col-sm-7 col-xs-7">
                                                <div class="contentBasked">
                                                    <p class="contentBasked__tittle">
                                                        <a href="<?php echo $product['href']; ?>">
                                                            <?php echo $product['name']; ?>
                                                        </a> <span>Denis</span>
                                                    </p>
                                                    <p class="contentBasked__text">
                                                        Модель : <span><?= $product['model'] ?></span>
                                                    </p>
                                                    <?php if ($product['option']) { ?>
                                                        <?php foreach ($product['option'] as $option) { ?>
                                                            <p class="contentBasked__text">
                                                                <?php echo $option['name']; ?> :
                                                                <span><?php echo $option['value']; ?></span>
                                                            </p>
                                                        <?php } ?>
                                                    <?php } ?>
                                                    <div class="wrapBtn wrapBtn--Basced">
                                                        <button class="wrapBtn__left" onclick="cart.update(<?= $product['cart_id']?>, <?php echo $product['quantity'] - 1; ?>)">-</button>
                                                        <input type="text" class="wrapBtn__texInp"
                                                               placeholder="1"
                                                               value="<?php echo $product['quantity']; ?>">
                                                        <button class="wrapBtn__right" onclick="cart.update(<?= $product['cart_id']?>, <?php echo $product['quantity'] + 1; ?>)">+</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-5 col-sm-5 col-xs-5">
                                        <div class="priceBasked">
                                            <p class="priceBasked__text"><?php echo $product['total']; ?></p>
                                            <span class="priceBasked__delete" onclick="cart.remove('<?php echo $product['cart_id'];  ?>');"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="wrapResascet">
                        <p class="resText"><?php echo $totals[count($totals) - 1]['title']; ?>:</p>
                        <p class="priceRes"><?php echo $totals[count($totals) - 1]['text']; ?></p>
                    </div>
                    <a href="<?php echo $checkout; ?>" class="blocComents__btn">
                        <?php echo $text_checkout; ?>
                    </a>
                    <a href="/" class="nextProuct">Продолжить покупки</a>
                <?php } ?>
            </div>

        </div>
    </div>
</div>