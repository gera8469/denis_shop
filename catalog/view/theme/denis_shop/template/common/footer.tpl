<section>
    <div class="blockEmail">
        <div class="container">
            <h4 class="blockEmail__tittle">
                Получите Наши <span>специальные предложения</span>
                И секреты ухода за ногтями, советы и хитрости!
            </h4>
            <p class="blockEmail__text">
                Отправьте нам. ваш EMAIL и мы пришлем вам PDF с секретной информацией.
            </p>
            <form action="#" class="emailto">
                <input type="email" name="indexEmail" class="email" placeholder="Введите Ваш email">
                <input type="submit" class="emailto__submit podrob" value="ОТПРАВИТЬ">
            </form>
            <p class="blockEmail__tittleSocial">Давайте оставаться на связи</p>
            <div class="wrapSocialMail">
                <div class="yu"><a href="#">
                        <div class="soc-vk"></div>
                    </a></div>
                <div class="in"><a href="#">
                        <div class="soc-in"></div>
                    </a></div>
                <div class="tw"><a href="#">
                        <div class="soc-wa"></div>
                    </a></div>
            </div>
        </div>
        <img src="/catalog/view/theme/denis_shop/image/main_page/blokEmail.png" alt="img" class="blockEmail__img">
    </div>
</section>

<?php if (!empty($categories)) {
    $half = round(count($categories)/ 2);
    ?>
    <section>
        <h5 class="hidden">hid</h5>
        <div class="blockServises">
            <div class="container">
                <div class="row">
                    <?php
                    $toPrintTitle = true;
                    foreach (array_chunk($categories, $half) as $categoryBlock) { ?>
                        <div class="col-md-4">
                            <ul class="servises">
                                <?php if ($toPrintTitle) { ?>
                                    <li class="servises__tittle">КАТАЛОГ ТОВАРОВ</li>
                                    <?php
                                    $toPrintTitle = false;
                                } ?>
                                <?php foreach ($categoryBlock as $category) { ?>
                                    <li>
                                        <a href="<?php echo $category['href']; ?>">
                                            <?php echo $category['name']; ?>
                                        </a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    <?php } ?>
                    <div class="col-md-4">
                        <ul class="servises servises--help">
                            <li class="servises__tittle">ПОМОЩЬ</li>
                            <li><a href="#">Denis club</a></li>
                            <li><a href="#">партнерам</a></li>
                            <li><a href="#">оплата и доставка</a></li>
                            <li><a href="#">Изменение и возврат</a></li>
                            <li><a href="#">Вопросы-Ответы</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php }?>



<footer>
    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <?php if ($logo) { ?>
                        <?php if ($home == $og_url) { ?>
                            <img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>"
                                 class="logo"/>
                        <?php } else { ?>
                            <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>"
                                                                alt="<?php echo $name; ?>" class="logo"/></a>
                        <?php } ?>
                    <?php } else { ?>
                        <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
                    <?php } ?>
<!--                    <a href="#"><img src="img/logo.png" alt="logo" class="footerLogo"></a>-->
                </div>
                <div class="col-md-6">
                    <div class="footerMenu">
                        <ul class="footerMenu__ul">
                            <li><a href="/">главная<i>/</i></a></li>
                            <li><a href="/index.php?route=information/information&information_id=4">О нас<i>/</i></a></li>
                            <li><a href="/index.php?route=information/information&information_id=7">блог<i>/</i></a></li>
                            <li><a href="/index.php?route=information/contact">контакты</a></li>
                        </ul>
                        <p class="copyring">
                            © 2017 EVERYTHING. All Right Reserved. PrestaShop Themes by PresThemes.com
                        </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <ul class="contact">
                        <li class="contact__tittle">Denic Shop</li>
                        <li>г. Киев</li>
                        <li><a href="mailto:denicshop@gmail.com">denicshop@gmail.com</a></li>
                        <li><a href="tel:0970998411">097-099-84-11</a>,<a href="tel:0970998411">097-099-84-11</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>

<div class="modal fade" id="product_info_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

            </div>

            <div class="modal-body">
                <div id="product_info_container" class="containerpop"></div>
            </div>

        </div>
    </div>
</div>


<!--[if lt IE 9]>
<script src="/catalog/view/theme/denis_shop/libs/html5shiv/es5-shim.min.js"></script>
<script src="/catalog/view/theme/denis_shop/libs/html5shiv/html5shiv.min.js"></script>
<script src="/catalog/view/theme/denis_shop/libs/html5shiv/html5shiv-printshiv.min.js"></script>
<script src="/catalog/view/theme/denis_shop/libs/respond/respond.min.js"></script>
<![endif]-->

<!--<script src="/catalog/view/theme/denis_shop/libs/jquery/jquery-1.11.1.min.js"></script>-->
<!-- cookie -->
<!--<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>-->


<script src="/catalog/view/theme/denis_shop/libs/jquery-cookie-master/src/jquery.cookie.js"></script>
<script src="/catalog/view/theme/denis_shop/libs/scrollto/jquery.scrollTo.min.js"></script>
<script src="/catalog/view/theme/denis_shop/libs/crros-browser/cross-browser.js"></script>
<script src="/catalog/view/theme/denis_shop/bower_components/jquery.form-styler/dist/jquery.formstyler.js"></script>
<script src="/catalog/view/theme/denis_shop/bower_components/slick-carousel/slick/slick.js"></script>
<script src="/catalog/view/theme/denis_shop/bower_components/magnific-popup/dist/jquery.magnific-popup.js"></script>
<script src="/catalog/view/theme/denis_shop/bower_components/jquery.form-styler/dist/jquery.formstyler.js"></script>
<script src="/catalog/view/theme/denis_shop/bower_components/bootstrap/js/modal.js"></script>
<script src="/catalog/view/theme/denis_shop/bower_components/bootstrap/js/tab.js"></script>
<script src="/catalog/view/theme/denis_shop/bower_components/jquery-validation/dist/jquery.validate.js"></script>
<!--<script src="/catalog/view/theme/denis_shop/bower_components/air-datepicker/dist/js/datepicker.js"></script>-->
<script src="/catalog/view/theme/denis_shop/bower_components/jquery.maskedinput/dist/jquery.maskedinput.js"></script>
<script src="/catalog/view/theme/denis_shop/libs/jquery-mousewheel/jquery.mousewheel.min.js"></script>
<script src="/catalog/view/theme/denis_shop/libs/nisescroll/jquery.nicescroll.min.js"></script>
<script src="/catalog/view/theme/denis_shop/js/common/common.js"></script>


<?php if (!empty($scripts)) { ?>
    <?php foreach ($scripts as $script) { ?>
        <script src="<?= $script ?>" ></script>
    <?php } ?>
<?php } ?>

<script>
    // tabs
    function handlerTabs(e) {
        e.preventDefault();
        var item = $(this).closest('.blockTabsTabs__controls'),
            content = $('.tab'),
            itemPosition = item.index();

        content.eq(itemPosition)
            .add(item)
            .addClass('activeTab')
            .addClass('activeTab2')
            .siblings()
            .removeClass('activeTab')
            .removeClass('activeTab2');


    }

    $('.blockTabsTabs__controls ').click(handlerTabs);
    (function ($) {
      $(function () {
        $('input,select').styler();
      });
    })(jQuery);
</script>


<!--
OpenCart is open source software and you are free to remove the powered by OpenCart if you want, but its generally accepted practise to make a small donation.
Please donate via PayPal to donate@opencart.com
//-->


<!-- Theme created by Welford Media for OpenCart 2.0 www.welfordmedia.co.uk -->

</body></html>