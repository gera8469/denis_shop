<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if lt IE 7]>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8 ]>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>
        <?php echo $title;
        if (isset($_GET['page'])) {
            echo " - " . ((int)$_GET['page']) . " " . $text_page;
        } ?>
    </title>
    <base href="<?php echo $base; ?>"/>
    <?php if ($description) { ?>
        <meta name="description" content="<?php echo $description;
        if (isset($_GET['page'])) {
            echo " - " . ((int)$_GET['page']) . " " . $text_page;
        } ?>"/>
    <?php } ?>
    <?php if ($keywords) { ?>
        <meta name="keywords" content="<?php echo $keywords; ?>"/>
    <?php } ?>
    <meta property="og:title" content="<?php echo $title;
    if (isset($_GET['page'])) {
        echo " - " . ((int)$_GET['page']) . " " . $text_page;
    } ?>"/>
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="<?php echo $og_url; ?>"/>
    <?php if ($og_image) { ?>
        <meta property="og:image" content="<?php echo $og_image; ?>"/>
    <?php } else { ?>
        <meta property="og:image" content="<?php echo $logo; ?>"/>
    <?php } ?>
    <meta property="og:site_name" content="<?php echo $name; ?>"/>
    <script src="/catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
<!--    <link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen"/>-->
<!--    <script src="/catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>-->
<!--    <link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>-->
<!--    <link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css"/>-->
<!--    <link href="catalog/view/theme/default/stylesheet/stylesheet.css" rel="stylesheet">-->
    <?php foreach ($styles as $style) { ?>
        <link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>"
              media="<?php echo $style['media']; ?>"/>
    <?php } ?>
    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet"
          type="text/css"/>
    <link rel="stylesheet" href="/catalog/view/theme/denis_shop/libs/font-awesome-4.2.0/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="/catalog/view/theme/denis_shop/css/style.css">
    <script src="/catalog/view/theme/denis_shop/libs/modernize/modernizr-2.8.3.min.js"></script>
    <script src="/catalog/view/javascript/common.js" type="text/javascript"></script>
    <script src="/catalog/view/theme/denis_shop/js/common/common.js" type="text/javascript"></script>
    <?php foreach ($links as $link) { ?>
        <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>"/>
    <?php } ?>
    <?php foreach ($scripts as $script) { ?>
        <script src="<?php echo $script; ?>" type="text/javascript"></script>
    <?php } ?>
    <?php foreach ($analytics as $analytic) { ?>
        <?php echo $analytic; ?>
    <?php } ?>
</head>
<body >
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->

<header>
    <div class="wrapTopHeader">
        <div class="topHeader topHeader--border">
            <div class="topHedLeft">
                <div class="topHedLeft__bars"><i class="fa fa-bars" aria-hidden="true"></i></div>
                <ul class="clikMenu">
                    <?php if ($logged) { ?>
                        <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
                        <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
                        <li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li>
                        <li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li>
                        <li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
                    <?php } else { ?>
                        <li><a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></li>
<!--                        <li><a href="--><?php //echo $login; ?><!--">--><?php //echo $text_login; ?><!--</a></li>-->
                        <li><a href="#form-popup" class="popup-with-form"><?php echo $text_login; ?></a></li>
                    <?php } ?>
                </ul>
            </div>
            <div class="container">
                <div class="col-md-4">
                    <div class="callbec">
                        <div class="callbec_wrapNumber">
                            <a href="<?php echo $contact; ?>" class="number"><?php echo $telephone; ?></a>
                        </div>
                        <p class="callbec__text">Ежедневно с 7:55 до 20:05</p>
                        <a href="#" class="callbec__calbec">ОБРАТНЫЙ ЗВОНОК</a>

                        <div class="callBacOpen callBacOpen--coloShadoPos" id="callBacOpen">
                            <p class="callBacOpen__tittle">мы перезвоним вам сами!</p>
                            <form action="#" method="post" id="callBacOpenForm">
                                <input type="text" class="callBacOpen__inp" name="callBacName"
                                       placeholder="Введите Ваше имя" required>
                                <input type="text" id="callBackphone" class="callBacOpen__inp" name="callBacPhone"
                                       placeholder="Телефон" required>
                                <textarea class="callBacMess" name="callBacMess"
                                          placeholder="Текст сообщения"></textarea>
                                <input type="submit" class="callBacOpen__send" value="ОТПРАВИТЬ">
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <ul class="top-nav">
                        <?php if ($logged) { ?>
                            <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
                        <?php } else { ?>
                            <li><a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></li>
                        <?php } ?>
                        <li><a href="#">denis club</a></li>
                        <li><a href="#">партнерам</a></li>
                        <li><a href="#">оплата и доставка</a></li>
                    </ul>
                </div>
                <div class="topHedRight">

                    <?php echo $cart; ?>

                    <?php echo $search; ?>
                </div>
            </div>
        </div>
    </div>

    <div class="middleHeader middleHeader--pos">
        <div class="container">
            <div id="logo">
                <?php if ($logo) { ?>
                    <?php if ($home == $og_url) { ?>
                        <img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>"
                             class="logo"/>
                    <?php } else { ?>
                        <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>"
                                                            alt="<?php echo $name; ?>" class="logo"/></a>
                    <?php } ?>
                <?php } else { ?>
                    <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
                <?php } ?>
            </div>
            <nav>
                <ul class="mainMenu">
                    <?php if ($categories) {
                        $half = round(count($categories) / 2);
                        ?>
                        <li class="mainMenu__li dropdown">
                            <a href="/index.php?route=product/category/categoryList">каталог товаров <i>/</i></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <div class="row">
                                        <?php foreach (array_chunk($categories, $half) as $categoryBlock) { ?>
                                            <div class="col-lg-6 col-md-6 dropdown--pad">
                                                <div class="leftTableMenu">
                                                    <ul class="wrap-menuLeft">
                                                <?php foreach ($categoryBlock as $category) { ?>
                                                    <li>
                                                        <a href="<?php echo $category['href']; ?>">
                                                            <span class="leftTableMenu__left-span">
                                                                <?php echo $category['name']; ?>
                                                            </span>
                                                        </a>
                                                    </li>
                                                <?php } ?>
                                                    </ul>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    <?php } ?>
                    <li class="mainMenu__li"><a href="/"> главная <i>/</i></a></li>
                    <li class="mainMenu__li"><a href="/index.php?route=information/information&information_id=4"> О нас <i>/</i></a></li>
                    <li class="mainMenu__li"><a href="/index.php?route=information/information&information_id=7">блог <i>/</i></a></li>
                    <li class="mainMenu__li"><a href="/index.php?route=information/contact"> контакты</a></li>
                </ul>
            </nav>
        </div>
    </div>
</header>

<div class="hidden">
    <!--modal register-->
    <div id="form-popup" class="white-popup mfp-hide">
        <div class="popupBannerRegister"><img src="/catalog/view/theme/denis_shop/image/common/bannerRegister.jpg" alt="banner"></div>
        <form class="formRegisterPopp" id="loginForm" action="#">
            <h3 class="formRegisterPopp__h3">Авторизация</h3>
            <p class="formRegisterPopp__tittle">
                Введите ваши регистрационные данные
                для входа в ваш личный кабинет
            </p>
            <div id="login-error" class="text-warning"></div>
            <input type="email" class="login" name="email" placeholder="Введите Ваш email">
            <input type="password" class="pass" name="password" placeholder="Введите Ваш пароль">
            <div class="wrap-form">

                <input id="checkRegister" type="checkbox" checked>
                <label for="checkRegister">Запомнить пароль</label>

                <input id="user_login" type="submit" class="enterPopup" value="ВОЙТИ">
            </div>
            <div class="wrapBottomFormRegister">
                <p class="wrapBottomFormRegister__text">
                    Вы еще не зарегистрированы?<br>
                    Пройти простую <a href="#">регистрацию</a>
                </p>
                <a href="#" class="wrapBottomFormRegister__rememberPass">Забыли пароль ?</a>
            </div>
        </form>

    </div>
    <!---->
    <!--modal exit-->
    <div id="form-popup2" class="white-popup2 mfp-hide">
        <div class="containerPopup">
            <h4 class="blockEmail__tittle">
                Получите Наши <span>специальные предложения</span>
                И секреты ухода за ногтями, советы и хитрости!
            </h4>
            <p class="blockEmail__text">
                Отправьте нам. ваш EMAIL и мы пришлем вам PDF с секретной информацией.
            </p>
            <form action="#" class="emailto">
                <input type="email" name="indexEmail" class="email" placeholder="Введите Ваш email">
                <input type="submit" class="emailto__submit podrob" value="ОТПРАВИТЬ">
            </form>
            <p class="blockEmail__tittleSocial">Давайте оставаться на свяи</p>
            <div class="wrapSocialMail">
                <div class="yu"><a href="#">
                        <div class="soc-vk"></div>
                    </a></div>
                <div class="in"><a href="#">
                        <div class="soc-in"></div>
                    </a></div>
                <div class="tw"><a href="#">
                        <div class="soc-wa"></div>
                    </a></div>
            </div>
        </div>

    </div>
    <!---->
</div>
