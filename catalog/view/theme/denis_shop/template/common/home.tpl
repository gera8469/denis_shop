<?php echo $header; ?>
    <!--<div class="container">-->
    <div class="row"><?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
            <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
            <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
            <?php $class = 'col-sm-12'; ?>
        <?php } ?>

        <!--    <div id="content" class="--><?php //echo $class; ?><!--">-->
        <?php echo $content_top; ?>
        <article>
            <!---->
            <h6 class="hidden">hid</h6>
            <!---->
            <div class="wrapBlocAction">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-4 col-md-4">
                            <a href="#" class="wrapBlocAction__hover-a wrapBlocAction--wiz">
                                <img src="/catalog/view/theme/denis_shop/image/main_page/action-img-1.jpg" alt="">
                            </a>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <a class="wrapBlocAction__hover-a" href="#">
                                <img src="/catalog/view/theme/denis_shop/image/main_page/action-img-2.jpg"
                                     class="wrapBlocAction__actionImg3 " alt="">
                            </a>
                            <a class="wrapBlocAction__hover-a" href="#">
                                <img src="/catalog/view/theme/denis_shop/image/main_page/action-img-3.jpg"
                                     class="wrapBlocAction__actionImg3 wrapBlocAction__actionImg3--pad" alt="">
                            </a>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <a class="wrapBlocAction__hover-a" href="#">
                                <img src="/catalog/view/theme/denis_shop/image/main_page/action-img-4.jpg" alt=""
                                     class="wrapBlocAction__actionImg4">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </article>
        <?php if (!empty($categories)) { ?>
            <article>
                <div class="blocCategories">
                    <div class="container">
                        <a href="/index.php?route=product/category/categoryList"><h2 class="blocCategories-h2"><span>ПРОСМОТР</span> КАТЕГОРИй товара</h2></a>
                        <div class="wrapLinearBlack">
                            <div class="wrapLinearBlack__bold"></div>
                        </div>
                        <div class="slicSlider">
                            <?php foreach ($categories as $category) { ?>
                                <div class="sliderCategories">
<!--                                    <a href="--><?//= $category['href']?><!--">-->
                                    <a href="/index.php?route=product/category/categoryList">
                                        <div class="wrapCategoriesItem">
                                            <div class="wrap-img">
                                                <div class="wrapCategoriesItem__mask"></div>
                                                <img src="<?= $category['thumb'] ?>" alt="img" class="wrapCategoriesItem__img">
                                            </div>
                                            <div class="itemLinearHover">
                                                <div class="itemLinearHover__bold"></div>
                                            </div>
                                            <div class="bloc-text">
                                                <p class="wrapCategoriesItem__text"><?= $category['name'] ?></p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </article>
        <?php } ?>



        <section>
            <div class="wrapMainBloc">
                <img src="/catalog/view/theme/denis_shop/image/main_page/imgDenis.png" alt="img"
                     class="wrapMainBloc__img wrapMainBloc__img--denis">
                <div class="container container--pos">
                    <div class="row">
                        <div class="col-lg-7 col-lg-offset-4">
                            <div class="linear linear--style">
                                <div class="linear__strong"></div>
                            </div>
                            <div class="wrapMainText">
                                <p class="wrapMainText__slogan">
                                    Украинская Торговая Марка <br>
                                    <span>«ДенIC»</span>
                                </p>
                                <h1 class="wrapMainText__h1">
                                    12 лет на
                                    рынке снг
                                </h1>
                                <p class="wrapMainText__dopText wrapMainText__dopText--denis">
                                    В сегменте недорогого качественного оборудования и
                                    материалов для парикмахерских и салонов красоты мы
                                    являемся одним из лидеров на рынках ряда стран СНГ и,
                                    в частности, в Украине...
                                </p>
                                <a href="/index.php?route=information/information&information_id=4" class="podrob">подробнее</a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>

        <?php echo $content_bottom; ?>






        <!--    </div>-->
        <?php echo $column_right; ?></div>
    <!--</div>-->
<?php echo $footer; ?>