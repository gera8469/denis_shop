<?php echo $header; ?>
<section>
    <div class="container">
        <div class="SencsText">
            <?php if (!empty($order_id)) { ?>
                <h1 class="SencsText__tittle">Спасибо за ваш заказ</h1>
                <p class="SencsText__text">
                    В ближайшее время наш менеджер свяжется с Вами для уточнения деталий заказа
                </p>
                <p class="SencsText__number">Номер вашего заказа: <?=$order_id?></p>
            <?php } else { ?>
                <h1 class="SencsText__tittle">Вы должны сделать заказ</h1>
            <?php } ?>

        </div>
    </div>
</section>
<?php echo $footer; ?>
