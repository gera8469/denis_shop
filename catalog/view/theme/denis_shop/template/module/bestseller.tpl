<section>
    <div class="sliderSale">
        <div class="container">
            <h3 class="blocCategories-h2"><span>Популярный</span>продукт</h3>
            <div class="wrapLinearBlack">
                <div class="wrapLinearBlack__bold"></div>
            </div>
            <div class="row">
                <div class="col-md-3 hidden-s">
                    <a href="#" class="twoslidBanner">
                        <img src="/catalog/view/theme/denis_shop/image/product/slider2-banner.png" alt="img">
                    </a>
                </div>
                <div class="col-md-9">
                    <div class="slicPopular slicPopular--item">
                        <?php
//                        myDump($products, false);
                        foreach ($products as $product) { ?>
                            <div class="sliderCategories itemA">
                                <div class="Item">
                                    <div class="wrapCategoriesItem wrapCategoriesItem--border">
                                        <div class="wrap-img">
                                            <div class="wrapCategoriesItem__mask wrapCategoriesItem__mask--col">
                                                <div class="wrapAItem">
                                                    <a href="javascript:;" class="add" onclick="cart.add('<?php echo $product['product_id']; ?>');">
                                                        Добавить
                                                    </a>
                                                    <a href="javascript:;" class="scale" data-toggle="tooltip"
                                                       title="<?php echo $button_wishlist; ?>"
                                                       onclick="wishlist.add('<?php echo $product['product_id']; ?>');">
                                                        <i class="fa fa-heart" aria-hidden="true"></i>
                                                    </a>
                                                    <a href="#" class="lupe product_info" data-product_id="<?= $product['product_id']?>">
                                                        <i class="fa fa-search" aria-hidden="true"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <img src="<?= $product['thumb']?>" alt="img">
                                            <span class="wrap-img__span wrap-img__span--popular">Popular</span>
                                        </div>
                                        <div class="bloc-text bloc-text--mod">
                                            <p class="bloc-text__nameCategorie">ХИТ</p>
                                            <p class="bloc-text__nameItem"><?php echo $product['name']; ?></p>
                                            <div class="star">
                                                <?php for ($i = 1; $i <= 5; $i++) {

                                                    $ratingClass = '';

                                                    if ($product['rating'] < $i) {
                                                        $ratingClass = 'star-star1';
                                                    } else {
                                                        $ratingClass = 'star-star3';
                                                    } ?>
                                                    <div class="<?= $ratingClass ?>"></div>
                                                <?php } ?>
                                            </div>
                                            <?php if ($product['price']) { ?>
                                                <?php if (!$product['special']) { ?>
                                                    <p class="bloc-text__price">
                                                        &#8372;<?php echo round($product['price']); ?>
                                                    </p>
                                                <?php } else { ?>
                                                    <p class="bloc-text__price">
                                                        &#8372;<?php echo round($product['special']); ?>
                                                        <span>&#8372;<?php echo round($product['price']); ?></span>
                                                    </p>
                                                <?php } ?>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>