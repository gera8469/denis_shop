<div class="category">
    <div class="blocLinearFillter">
        <div class="blocLinearFillter__bold"></div>
    </div>
    <p class="blocPriceNumber__name blocPriceNumber__name--mar">Категории</p>
    <ul class="sidebarNav">
    <?php foreach ($categories as $category) { ?>
        <li><a href="<?php echo $category['href']; ?>"><?= $category['name']?></a></li>
    <?php } ?>
</div>