<!--<form action="#">-->
    <div class="blocPrice">
        <div class="blocLinearFillter">
            <div class="blocLinearFillter__bold"></div>
        </div>
        <div class="blocPriceNumber">
            <p class="blocPriceNumber__name">цена</p>
            <span class="blocPriceNumber__ot">от</span>
            <input type="text" name="price[min]" id="ot"
                   value="<?= isset($min_price) ? $min_price : '' ?>"
                   class="blocPriceNumber__inp" maxlength="7">
            <span class="blocPriceNumber__ot">до</span>
            <input type="text" name="price[max]" id="do"
                   value="<?= isset($max_price) ? $max_price : '' ?>"
                   class="blocPriceNumber__inp" maxlength="7">
            <span class="blocPriceNumber__ot">грн</span>
            <button type="button" id="price_filter_button">OK</button>
        </div>
        <div class="polzun">
            <div id="slider"></div>
        </div>
        <p class="blocPriceNumber__resPrice">Цена: <span id="otRes">63</span> грн - <span
                    id="doRes">1500</span> грн</p>
    </div>
    <?php foreach ($filter_groups as $filter_group) { ?>
        <div class="blockBrands">
            <div class="blocLinearFillter">
                <div class="blocLinearFillter__bold"></div>
            </div>
            <p class="blocPriceNumber__name blocPriceNumber__name--mar"><?php echo $filter_group['name']; ?></p>
            <?php foreach ($filter_group['filter'] as $filter) { ?>
<!--                <label>-->
                    <?php if (in_array($filter['filter_id'], $filter_category)) { ?>
                        <input type="checkbox" name="filter[]" value="<?php echo $filter['filter_id']; ?>"
                               checked="checked"/>
                        <?php echo $filter['name']; ?>
                    <?php } else { ?>
                        <input type="checkbox" name="filter[]" value="<?php echo $filter['filter_id']; ?>"/>
                        <?php echo $filter['name']; ?>
                    <?php } ?>
<!--                </label>-->
            <?php } ?>
        </div>
    <?php } ?>
<!--</form>-->


<script>
    
    
    
    $('input[name^="filter"]').on('change', handleFilter);
    $('#price_filter_button').on('click', handleFilter);


    function handleFilter() {
        console.log('test');
        filter = [];

        $('input[name^=\'filter\']:checked').each(function (element) {
            filter.push(this.value);
        });

        location = '<?php echo $action; ?>&filter=' + filter.join(',') + '&min_price=' + $('#ot').val() + '&max_price=' + $('#do').val();
    }
    
</script>
