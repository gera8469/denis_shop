<section>
    <div class="wrap-sectionSkider">
        <div class="mainSlider">
            <?php foreach ($banners as $banner) { ?>
                <?php
                $isLink = false;

                if ($banner['link']) {
                    $isLink = true;
                } ?>

                <?php if ($isLink) {
                    echo "<a href='{$banner['link']}'>";
                } ?>
                <div class="wrapMainBloc">
                    <img src="<?php echo $banner['image']; ?>" alt="img" class="wrapMainBloc__img">
                    <div class="container container--pos">
                        <div class="row">
                            <div class="col-lg-7">
                                <div class="linear linear--style">
                                    <div class="linear__strong"></div>
                                </div>
                                <div class="wrapMainText">
                                    <p class="wrapMainText__slogan">
                                        <span>Получите</span> подарок - <span>100</span> грн
                                        от Denis club
                                    </p>
                                    <h1 class="wrapMainText__h1">
                                        denis club
                                    </h1>
                                    <p class="wrapMainText__dopText">
                                        Чтобы получить подарок 100 грн на первый заказ от
                                        торговой марки “ДенIC” - станьте участником
                                        программы DENIS CLUB, пройдя простую регистрацию
                                        на нашем сайте.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php if ($isLink) {
                    echo "</a>";
                } ?>
            <?php } ?>
        </div>
        <div class="wrapbtnSlider">
            <a href="/index.php?route=account/register&bonus=100" class="enter">получить 100 грн</a>
        </div>
    </div>
</section>