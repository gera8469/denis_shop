<section>
    <div class="sliderSale">
        <div class="container">
            <h2 class="blocCategories-h2"><span><?php echo $heading_title; ?></span></h2>
            <div class="wrapLinearBlack">
                <div class="wrapLinearBlack__bold"></div>
            </div>
            <div class="slicSale">
                <?php foreach ($products as $product) {
//                    myDump($product, false);
                    ?>
                    <div class="sliderCategories itemA">
                        <div class="Item">
                            <div class="wrapCategoriesItem wrapCategoriesItem--border">
                                <div class="wrap-img">
                                    <img src="<?php echo $product['thumb']; ?>"
                                         alt="<?php echo $product['name']; ?>"
                                         title="<?php echo $product['name']; ?>"/>
                                    <span class="wrap-img__span">Sale</span>
                                </div>
                                <div class="bloc-text bloc-text--mod">
                                    <p class="bloc-text__nameCategorie">РАСПРОДАЖА</p>
                                    <p class="bloc-text__nameItem"><?php echo $product['name']; ?></p>
                                    <?php if (!empty($product['rating']) || (int)$product['rating'] === 0) { ?>
                                        <div class="star">
                                            <?php for ($i = 1; $i <= 5; $i++) {

                                                $ratingClass = '';

                                                if ($product['rating'] < $i) {
                                                    $ratingClass = 'star-star1';
                                                } else {
                                                    $ratingClass = 'star-star3';
                                                } ?>
                                                <div class="<?= $ratingClass ?>"></div>
                                            <?php } ?>
                                        </div>
                                    <?php } ?>
                                    <?php if ($product['price']) { ?>
                                        <?php if (!$product['special']) { ?>
                                            <p class="bloc-text__price">
                                                &#8372;<?php echo $product['price']; ?>
                                            </p>
                                        <?php } else { ?>
                                            <p class="bloc-text__price">
                                                &#8372;<?php echo $product['special']; ?>
                                                <span>&#8372;<?php echo $product['price']; ?></span>
                                            </p>
                                        <?php } ?>
                                    <?php } ?>
                                </div>
                                <div class="wrapItemIt">
                                    <div class="wrapCategoriesItem wrapCategoriesItem--border">
                                        <div class="wrap-img">
                                            <div class="wrapCategoriesItem__mask wrapCategoriesItem__mask--col">
                                                <div class="wrapAItem">
                                                    <a href="javascript:;" class="add" onclick="cart.add('<?php echo $product['product_id']; ?>');">
                                                        Добавить
                                                    </a>
                                                    <a href="javascript:;" class="scale" data-toggle="tooltip"
                                                            title="<?php echo $button_wishlist; ?>"
                                                            onclick="wishlist.add('<?php echo $product['product_id']; ?>');">
                                                        <i class="fa fa-heart" aria-hidden="true"></i>
                                                    </a>
                                                    <a href="javascript:;" class="lupe product_info" data-product_id="<?= $product['product_id']?>">
                                                        <i class="fa fa-search" aria-hidden="true"></i>
                                                    </a>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="bloc-text bloc-text--mod">
                                            <p class="bloc-text__nameCategorie">РАСПРОДАЖА</p>
                                            <p class="bloc-text__nameItem"><?php echo $product['name']; ?></p>
                                            <?php if (!empty($product['rating']) || (int)$product['rating'] === 0) { ?>
                                                <div class="star">
                                                    <?php for ($i = 1; $i <= 5; $i++) {

                                                        $ratingClass = '';

                                                        if ($product['rating'] < $i) {
                                                            $ratingClass = 'star-star1';
                                                        } else {
                                                            $ratingClass = 'star-star3';
                                                        } ?>
                                                        <div class="<?= $ratingClass ?>"></div>
                                                    <?php } ?>
                                                </div>
                                            <?php } ?>
                                            <?php if ($product['price']) { ?>
                                                <?php if (!$product['special']) { ?>
                                                    <p class="bloc-text__price">
                                                        &#8372;<?php echo $product['price']; ?>
                                                    </p>
                                                <?php } else { ?>
                                                    <p class="bloc-text__price">
                                                        &#8372;<?php echo $product['special']; ?>
                                                        <span>&#8372;<?php echo $product['price']; ?></span>
                                                    </p>
                                                <?php } ?>
                                            <?php } ?>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</section>


<!--<h3><?php /*echo $heading_title; */?></h3>
<div class="row">
    <?php /*foreach ($products as $product) { */?>
        <div class="product-layout col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="product-thumb transition">
                <div class="image"><a href="<?php /*echo $product['href']; */?>"><img src="<?php /*echo $product['thumb']; */?>"
                                                                                  alt="<?php /*echo $product['name']; */?>"
                                                                                  title="<?php /*echo $product['name']; */?>"
                                                                                  class="img-responsive"/></a></div>
                <div class="caption">
                    <h4><a href="<?php /*echo $product['href']; */?>"><?php /*echo $product['name']; */?></a></h4>
                    <p><?php /*echo $product['description']; */?></p>
                    <?php /*if ($product['rating']) { */?>
                        <div class="rating">
                            <?php /*for ($i = 1; $i <= 5; $i++) { */?>
                                <?php /*if ($product['rating'] < $i) { */?>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                <?php /*} else { */?>
                                    <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i
                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                <?php /*} */?>
                            <?php /*} */?>
                        </div>
                    <?php /*} */?>
                    <?php /*if ($product['price']) { */?>
                        <p class="price">
                            <?php /*if (!$product['special']) { */?>
                                <?php /*echo $product['price']; */?>
                            <?php /*} else { */?>
                                <span class="price-new"><?php /*echo $product['special']; */?></span> <span
                                        class="price-old"><?php /*echo $product['price']; */?></span>
                            <?php /*} */?>
                            <?php /*if ($product['tax']) { */?>
                                <span class="price-tax"><?php /*echo $text_tax; */?><?php /*echo $product['tax']; */?></span>
                            <?php /*} */?>
                        </p>
                    <?php /*} */?>
                </div>
                <div class="button-group">
                    <button type="button" onclick="cart.add('<?php /*echo $product['product_id']; */?>');"><i
                                class="fa fa-shopping-cart"></i> <span
                                class="hidden-xs hidden-sm hidden-md"><?php /*echo $button_cart; */?></span></button>
                    <button type="button" data-toggle="tooltip" title="<?php /*echo $button_wishlist; */?>"
                            onclick="wishlist.add('<?php /*echo $product['product_id']; */?>');"><i class="fa fa-heart"></i>
                    </button>
                    <button type="button" data-toggle="tooltip" title="<?php /*echo $button_compare; */?>"
                            onclick="compare.add('<?php /*echo $product['product_id']; */?>');"><i
                                class="fa fa-exchange"></i></button>
                </div>
            </div>
        </div>
    <?php /*} */?>
</div>
-->