<?php echo $header; ?>
<section>
    <div class="blockTittle">
        <img src="/catalog/view/theme/denis_shop/image/category/img.png" alt="img" class="blockTittle__img">
        <div class="container">
            <div class="blockTittlLine">
                <div class="blockTittlLine__bold"></div>
            </div>
            <h1 class="blockTittle__h1">
                <span>регистрируйся</span> и
                получи <span>дисконтную</span> карту и
                <span>скидку 10%</span> уже сегодня на
                весь товар до конца месяца
            </h1>
        </div>
    </div>
</section>


<section>
    <div class="container">
        <div class="bredcrums">
            <ul class="bredcrums__ul">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li>
                        <a class="<?= !empty($breadcrumb['isActive']) && $breadcrumb['isActive'] ? 'bredcrums__active' : ''?>" href="<?php echo $breadcrumb['href']; ?>">
                            <?php echo $breadcrumb['text']; ?>
                            <?= empty($breadcrumb['isLast']) || !$breadcrumb['isLast'] ? '<i>/</i>' : ''?>
                        </a>
                    </li>
                <?php } ?>
            </ul>
        </div>
        <button class="filterOpen">Фильтр <span>+</span></button>
        <div class="row">
            <div class="col-md-4">
                <aside class="aside">
                    <?php echo $column_left; ?>
                    <form action="/index.php" method="get">
                        <div class="searchSidebar">
                            <div class="blocLinearFillter">
                                <div class="blocLinearFillter__bold"></div>
                            </div>
                            <p class="blocPriceNumber__name blocPriceNumber__name--mar">поиск</p>
                            <input type="search" name="search" placeholder="Поиск...">
                            <input type="hidden" name="route" value="product/search">
                        </div>
                    </form>
                    <div class="sidebarBanners">
                        <a href="#" class="bannerSidebar">
                            <div class="wrapImgBanner wrapImgBanner--mar">
                                <div class="wrapImgNews__maskBanner"></div>
                                <img src="/catalog/view/theme/denis_shop/image/category/sidebarBanner-1.jpg" alt="sidebarBanner">
                            </div>
                        </a>
                        <a href="#" class="bannerSidebar">
                            <div class="wrapImgBanner">
                                <div class="wrapImgNews__maskBanner"></div>
                                <img src="/catalog/view/theme/denis_shop/image/category/sidebarBanner-2.jpg" alt="sidebarBanner">
                            </div>
                        </a>

                    </div>
                </aside>
            </div>
            <div class="col-md-8">
                <div class="mainBanner">
                    <img src="/catalog/view/theme/denis_shop/image/category/stat.jpg" alt="banner">
                </div>
                <div class="mainFiltr">
                    <h2 class="tittleFilter">гели для наращивания</h2>
                    <div class="selectMainFiltr">
                        <select id="input-sort" onchange="location = this.value;">
                            <?php foreach ($sorts as $sorts) { ?>
                                <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
                                    <option value="<?php echo $sorts['href']; ?>"
                                            selected="selected"><?php echo $sorts['text']; ?></option>
                                <?php } else { ?>
                                    <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                    </div>
                    <hr>
                    <div class="bottomMainFiltr">

                    </div>

                </div>
                <?php if ($products) { ?>
                    <div class="items">
                        <div class="row">
                            <?php
                            $products = array_chunk($products, 2);
                            foreach ($products as $productBlock) { ?>
                                <div class="row">
                                    <?php foreach ($productBlock as $product) { ?>
                                        <div class="col-md-6">
                                            <div class="sliderCategories itemA">
                                                <div class="Item">
                                                    <div class="wrapCategoriesItem wrapCategoriesItem--border">
                                                        <div class="wrap-img">
                                                            <img src="<?= $product['thumb'] ?>" alt="img">
                                                            <span class="wrap-img__span">Sale</span>
                                                        </div>
                                                        <div class="bloc-text bloc-text--mod">
                                                            <?php if ($product['price'] && $product['special']) { ?>
                                                                <p class="bloc-text__nameCategorie">РАСПРОДАЖА</p>
                                                            <?php } ?>
                                                            <p class="bloc-text__nameItem"><?= $product['name'] ?></p>
                                                            <?php if (!empty($product['rating']) || (int)$product['rating'] === 0) { ?>
                                                                <div class="star">
                                                                    <?php for ($i = 1; $i <= 5; $i++) {

                                                                        $ratingClass = '';

                                                                        if ($product['rating'] < $i) {
                                                                            $ratingClass = 'star-star1';
                                                                        } else {
                                                                            $ratingClass = 'star-star3';
                                                                        } ?>
                                                                        <div class="<?= $ratingClass ?>"></div>
                                                                    <?php } ?>
                                                                </div>
                                                            <?php } ?>
                                                            <?php if ($product['price']) { ?>
                                                                <?php if (!$product['special']) { ?>
                                                                    <p class="bloc-text__price">
                                                                        &#8372;<?php echo $product['price']; ?>
                                                                    </p>
                                                                <?php } else { ?>
                                                                    <p class="bloc-text__price">
                                                                        &#8372;<?php echo $product['special']; ?>
                                                                        <span>&#8372;<?php echo $product['price']; ?></span>
                                                                    </p>
                                                                <?php } ?>
                                                            <?php } ?>
                                                        </div>
                                                        <div class="wrapItemIt">
                                                            <div class="wrapCategoriesItem wrapCategoriesItem--border">
                                                                <div class="wrap-img">
                                                                    <div class="wrapCategoriesItem__mask wrapCategoriesItem__mask--col">
                                                                        <div class="wrapAItem">
                                                                            <a href="javascript:;" class="add" onclick="cart.add('<?php echo $product['product_id']; ?>');">
                                                                                Добавить
                                                                            </a>
                                                                            <a href="javascript:;" class="scale" data-toggle="tooltip"
                                                                               title="<?php echo $button_wishlist; ?>"
                                                                               onclick="wishlist.add('<?php echo $product['product_id']; ?>');">
                                                                                <i class="fa fa-heart" aria-hidden="true"></i>
                                                                            </a>
                                                                            <a href="#" class="lupe product_info" data-product_id="<?= $product['product_id']?>">
                                                                                <i class="fa fa-search" aria-hidden="true"></i>
                                                                            </a>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                                <div class="bloc-text bloc-text--mod">
                                                                    <?php if ($product['price'] && $product['special']) { ?>
                                                                        <p class="bloc-text__nameCategorie">РАСПРОДАЖА</p>
                                                                    <?php } ?>
                                                                    <p class="bloc-text__nameItem"><?php echo $product['name']; ?></p>
                                                                    <?php if (!empty($product['rating']) || (int)$product['rating'] === 0) { ?>
                                                                        <div class="star">
                                                                            <?php for ($i = 1; $i <= 5; $i++) {

                                                                                $ratingClass = '';

                                                                                if ($product['rating'] < $i) {
                                                                                    $ratingClass = 'star-star1';
                                                                                } else {
                                                                                    $ratingClass = 'star-star3';
                                                                                } ?>
                                                                                <div class="<?= $ratingClass ?>"></div>
                                                                            <?php } ?>
                                                                        </div>
                                                                    <?php } ?>
                                                                    <?php if ($product['price']) { ?>
                                                                        <?php if (!$product['special']) { ?>
                                                                            <p class="bloc-text__price">
                                                                                &#8372;<?php echo $product['price']; ?>
                                                                            </p>
                                                                        <?php } else { ?>
                                                                            <p class="bloc-text__price">
                                                                                &#8372;<?php echo $product['special']; ?>
                                                                                <span>&#8372;<?php echo $product['price']; ?></span>
                                                                            </p>
                                                                        <?php } ?>
                                                                    <?php } ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="blocAdd">
                            <div class="row">
                                <div><?php echo $pagination; ?></div>
                                <div></div>
                                <div><?php echo $results; ?></div>
                            </div>
                        </div>
                    </div>
                <?php } ?>

            </div>
        </div>
    </div>
    <?php echo $content_bottom; ?>
    <?php echo $column_right; ?>
</section>


<?php echo $footer; ?>
