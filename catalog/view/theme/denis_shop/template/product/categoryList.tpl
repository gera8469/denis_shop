<?= $header?>

<section>
    <div class="blockTittle">
        <img src="/catalog/view/theme/denis_shop/image/category/img.png" alt="img" class="blockTittle__img">
        <div class="container">
            <div class="blockTittlLine">
                <div class="blockTittlLine__bold"></div>
            </div>
            <h1 class="blockTittle__h1">
                <span>регистрируйся</span> и
                получи <span>дисконтную</span> карту и
                <span>скидку 10%</span> уже сегодня на
                весь товар до конца месяца
            </h1>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <div class="bredcrums">
            <ul class="bredcrums__ul">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li>
                        <a class="<?= !empty($breadcrumb['isActive']) && $breadcrumb['isActive'] ? 'bredcrums__active' : ''?>" href="<?php echo $breadcrumb['href']; ?>">
                            <?php echo $breadcrumb['text']; ?>
                            <?= empty($breadcrumb['isLast']) || !$breadcrumb['isLast'] ? '<i>/</i>' : ''?>
                        </a>
                    </li>
                <?php } ?>
            </ul>
        </div>
        <h2 class="blocCategories-h2"><span>ПРОСМОТР</span> КАТЕГОРИй товара</h2>
        <div class="wrapLinearBlack">
            <div class="wrapLinearBlack__bold"></div>
        </div>

        <?php if(!empty($categories)) { ?>
            <div class="slicSliderCategory">
                <div class="slip-pad">
                    <?php
//                    myDump($categories);
                    foreach (array_chunk($categories, 4) as $categoryBlock) { ?>
                        <div class="row">
                            <?php foreach ($categoryBlock as $category) { ?>
                                <div class="col-md-3 col-sm-6  noPad">
                                    <a class="disp" href="<?= $category['href']?>">
                                        <div class="wrapCategoriesItem">
                                            <div class="wrap-img">
                                                <div class="wrapCategoriesItem__mask"></div>
                                                <img src="<?= $category['thumb']?>" alt="img" class="wrapCategoriesItem__img">
                                            </div>
                                            <div class="itemLinearHover">
                                                <div class="itemLinearHover__bold"></div>
                                            </div>
                                            <div class="bloc-text bloc-text--border">
                                                <p class="wrapCategoriesItem__text"><?= $category['name']?></p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        <?php } ?>


    </div>
</section>

<?= $footer?>
