<?php echo $header; ?>
<section>
    <div class="blockTittle">
        <img src="/catalog/view/theme/denis_shop/image/category/img.png" alt="img" class="blockTittle__img">
        <div class="container">
            <div class="blockTittlLine">
                <div class="blockTittlLine__bold"></div>
            </div>
            <h1 class="blockTittle__h1">
                <span>регистрируйся</span> и
                получи <span>дисконтную</span> карту и
                <span>скидку 10%</span> уже сегодня на
                весь товар до конца месяца
            </h1>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <div class="bredcrums">
            <ul class="bredcrums__ul">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li>
                        <a class="<?= !empty($breadcrumb['isActive']) && $breadcrumb['isActive'] ? 'bredcrums__active' : ''?>" href="<?php echo $breadcrumb['href']; ?>">
                            <?php echo $breadcrumb['text']; ?>
                            <?= empty($breadcrumb['isLast']) || !$breadcrumb['isLast'] ? '<i>/</i>' : ''?>
                        </a>
                    </li>
                <?php } ?>
            </ul>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="blocSlidershow">
                    <div class="mainBlocImg">
                        <a href="#" class="mainBlocImg__plus">+</a>
                        <img src="<?= $thumb ?>" alt="<?= $name ?>">
                    </div>
                    <?php if ($thumb || $images) { ?>
                        <ul class="navMainSlidershow">

                            <?php if ($thumb) { ?>
                                <li class="navMainSlidershow__aImg">
                                    <a href="#">
                                        <img src="<?php echo $thumb; ?>" alt="<?=$name?>">
                                    </a>
                                </li>
                            <?php } ?>
                            <?php if ($images) { ?>
                                <?php foreach ($images as $image) { ?>
                                    <li class="navMainSlidershow__aImg">
                                        <a href="#">
                                            <img src="<?php echo $image['popup']; ?>" alt="<?=$name?>">
                                        </a>
                                    </li>
                                <?php } ?>
                            <?php } ?>
                        </ul>
                    <?php } ?>
                </div>
            </div>
            <div class="col-md-6 padNo">
                <div class="blocOpItem">
                    <span class="blocOpItem__popular">Popular</span>
                    <h3 class="blocCategories-h2 blocCategories-h2--item">
                        <span><?=$heading_title?></span> Denis
                    </h3>
                    <div class="wrapLinearBlack wrapLinearBlack--pad wrapLinearBlack--padItem">
                        <div class="wrapLinearBlack__bold"></div>
                    </div>
                    <div class="options options--border">
                        <p class="options__text">
                            <span>Модель:</span><?= $model?>
                        </p>
                        <p class="options__text">
                            <span>Наличие:</span><?=$stock?>
                        </p>
                    </div>
                    <div id="product">
                        <?php if (!empty($options)) { ?>
                            <?php foreach ($options as $option) { ?>
                                <?php if ($option['type'] == 'select') { ?>
                                    <div class="<?php echo($option['required'] ? ' required' : ''); ?>">
                                        <label class="" for="input-option<?php echo $option['product_option_id']; ?>">
                                            <?php echo $option['name']; ?>
                                        </label>
                                        <select name="option[<?php echo $option['product_option_id']; ?>]"
                                                id="input-option<?php echo $option['product_option_id']; ?>"
                                                class="form-control">
                                            <option value="">
                                                <?php echo $text_select; ?>
                                            </option>
                                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                <option value="<?php echo $option_value['product_option_value_id']; ?>">
                                                    <?php echo $option_value['name']; ?>
                                                    <?php if ($option_value['price']) { ?>
                                                        (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                    <?php } ?>
                                                </option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                <?php } ?>
                                <?php if ($option['type'] == 'radio') { ?>
                                    <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
                                        <label class="control-label"><?php echo $option['name']; ?></label>
                                        <div id="input-option<?php echo $option['product_option_id']; ?>">
                                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio"
                                                               name="option[<?php echo $option['product_option_id']; ?>]"
                                                               value="<?php echo $option_value['product_option_value_id']; ?>"/>
                                                        <?php echo $option_value['name']; ?>
                                                        <?php if ($option_value['price']) { ?>
                                                            (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                        <?php } ?>
                                                    </label>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php if ($option['type'] == 'checkbox') { ?>
                                    <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
                                        <label class="control-label"><?php echo $option['name']; ?></label>
                                        <div id="input-option<?php echo $option['product_option_id']; ?>">
                                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox"
                                                               name="option[<?php echo $option['product_option_id']; ?>][]"
                                                               value="<?php echo $option_value['product_option_value_id']; ?>"/>
                                                        <?php echo $option_value['name']; ?>
                                                        <?php if ($option_value['price']) { ?>
                                                            (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                        <?php } ?>
                                                    </label>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php if ($option['type'] == 'image') { ?>
                                    <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
                                        <label class="control-label"><?php echo $option['name']; ?></label>
                                        <div id="input-option<?php echo $option['product_option_id']; ?>">
                                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio"
                                                               name="option[<?php echo $option['product_option_id']; ?>]"
                                                               value="<?php echo $option_value['product_option_value_id']; ?>"/>
                                                        <img src="<?php echo $option_value['image']; ?>"
                                                             alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>"
                                                             class="img-thumbnail"/> <?php echo $option_value['name']; ?>
                                                        <?php if ($option_value['price']) { ?>
                                                            (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                        <?php } ?>
                                                    </label>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php if ($option['type'] == 'text') { ?>
                                    <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
                                        <label class="control-label"
                                               for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                        <input type="text" name="option[<?php echo $option['product_option_id']; ?>]"
                                               value="<?php echo $option['value']; ?>"
                                               placeholder="<?php echo $option['name']; ?>"
                                               id="input-option<?php echo $option['product_option_id']; ?>"
                                               class="form-control"/>
                                    </div>
                                <?php } ?>
                                <?php if ($option['type'] == 'textarea') { ?>
                                    <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
                                        <label class="control-label"
                                               for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                        <textarea name="option[<?php echo $option['product_option_id']; ?>]" rows="5"
                                                  placeholder="<?php echo $option['name']; ?>"
                                                  id="input-option<?php echo $option['product_option_id']; ?>"
                                                  class="form-control"><?php echo $option['value']; ?></textarea>
                                    </div>
                                <?php } ?>
                                <?php if ($option['type'] == 'file') { ?>
                                    <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
                                        <label class="control-label"><?php echo $option['name']; ?></label>
                                        <button type="button"
                                                id="button-upload<?php echo $option['product_option_id']; ?>"
                                                data-loading-text="<?php echo $text_loading; ?>"
                                                class="btn btn-default btn-block"><i
                                                    class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
                                        <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]"
                                               value="" id="input-option<?php echo $option['product_option_id']; ?>"/>
                                    </div>
                                <?php } ?>
                                <?php if ($option['type'] == 'date') { ?>
                                    <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
                                        <label class="control-label"
                                               for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                        <div class="input-group date">
                                            <input type="text"
                                                   name="option[<?php echo $option['product_option_id']; ?>]"
                                                   value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD"
                                                   id="input-option<?php echo $option['product_option_id']; ?>"
                                                   class="form-control"/>
                                            <span class="input-group-btn">
                <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                </span></div>
                                    </div>
                                <?php } ?>
                                <?php if ($option['type'] == 'datetime') { ?>
                                    <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
                                        <label class="control-label"
                                               for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                        <div class="input-group datetime">
                                            <input type="text"
                                                   name="option[<?php echo $option['product_option_id']; ?>]"
                                                   value="<?php echo $option['value']; ?>"
                                                   data-date-format="YYYY-MM-DD HH:mm"
                                                   id="input-option<?php echo $option['product_option_id']; ?>"
                                                   class="form-control"/>
                                            <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span></div>
                                    </div>
                                <?php } ?>
                                <?php if ($option['type'] == 'time') { ?>
                                    <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
                                        <label class="control-label"
                                               for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                        <div class="input-group time">
                                            <input type="text"
                                                   name="option[<?php echo $option['product_option_id']; ?>]"
                                                   value="<?php echo $option['value']; ?>" data-date-format="HH:mm"
                                                   id="input-option<?php echo $option['product_option_id']; ?>"
                                                   class="form-control"/>
                                            <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span></div>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                        <?php } ?>
                        <div class="blocPrice blocPrice--pad">
                            <p class="blocPrice__price">
                                &#8372; <?=round($price)?>
                            </p>
                            <div class="wrapBtn">
                                <button class="wrapBtn__left" onclick="productHandler.changeQuantityValue(-1, <?php echo $minimum; ?>)">-</button>
                                <input type="text" class="wrapBtn__texInp" name="quantity" value="<?php echo $minimum; ?>" size="2"
                                       id="input-quantity" class="form-control">
                                <button class="wrapBtn__right" onclick="productHandler.changeQuantityValue(1, <?php echo $minimum; ?>)">+</button>
                                <input type="hidden" name="product_id" value="<?php echo $product_id; ?>"/>
                            </div>
                            <div class="btnLike">
                                <button ><i class="fa fa-heart" aria-hidden="true" onclick="productHandler.addToWishList('<?php echo $product_id; ?>');"></i></button>
                            </div>
                        </div>
                    </div>



                    <br/>
                    <button type="button" id="button-cart" class="submitIte">
                        ДОБАВИТЬ В КОРЗИНУ
                    </button>

                    <p class="options__text2">
                        Доставка товара по Киеву - бесплатно курьером. В другие города Украины -
                        отправка товара по Новой Почте наложенным платежем. При заказе от 1500 грн
                        доставка будет бесплатной в любой регион. Также принимайте участие в
                        клиентской программе DENIS CLUB, чтобы получать скидки, подарки,
                        дисконтную карту.
                    </p>
                </div>
            </div>
        </div>

        <div class="blockTabs">
            <ul class="blockTabsTabs">
                <li class=" blockTabsTabs__controls activeTab"><a href="#">ОПИСАНИЕ</a>
                    <div class="activeTab__linear"></div>
                </li>
                <li class="blockTabsTabs__controls"><a href="#">ДОПОЛНИТЕЛЬНАЯ ИНФОРМАЦИЯ</a>
                    <div class="activeTab__linear"></div>
                </li>
            </ul>

            <div class="blocTabsContent">
                <div class=" tab tab-1 activeTab">
                    <h1 class="tab__h1">описание продукта</h1>
                    <?php if($description) { ?>
                        <?= $description ?>
                    <?php } ?>
                </div>
                <div class=" tab tab-2">
                    <?php if ($attribute_groups) { ?>
                        <table class="tableItems">
                            <?php foreach ($attribute_groups as $attribute_group) { ?>
                                <thead>
                                <tr>
                                    <td colspan="2"><strong><?php echo $attribute_group['name']; ?></strong>
                                    </td>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                                    <tr>
                                        <td class="tableItems__left"><?php echo $attribute['name']; ?></td>
                                        <td class="tableItems__right"><?php echo $attribute['text']; ?></td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            <?php } ?>
                        </table>
                    <?php } ?>
                </div>
            </div>
        </div>

        <?php if ($review_status) { ?>
            <div class="blocComents">
                <h2 class="tab__h1 tab__h1--center">Отзывы об этом продукте</h2>
                <div class="linearCenter">
                    <div class="linearCenter__bold"></div>
                </div>
                <?php if ($review_guest) { ?>
                    <a href="#" id="coments" class="blocComents__btn">НАПИСАТЬ СВОЙ КОММЕНТАРИЙ</a>
                    <div class="comentsPopUp">
                            <div class="comentsPopUp__wrap">
                                <button class="closePopup"><i class="fa fa-times" aria-hidden="true"></i></button>
                                <div class="row">
                                    <form id="form-review">
                                        <div id="review-result"></div>
                                        <div class="col-md-4">
                                            <div class="leftComentPopup leftComentPopup--pad">
                                                <p class="leftComentPopup__text1">
                                                    Вы можете оставить отзыв
                                                </p>
                                                <p class="leftComentPopup__text2">
                                                    Используйте данную форму только для
                                                    того, чтобы оставить отзыв о товаре.
                                                </p>
                                                <p class="leftComentPopup__text2">
                                                    Все комментарии не касающиеся отзывов о
                                                    продукте будут удалены!
                                                </p>
                                                <p class="leftComentPopup__text1">
                                                    Комментарий будет выведен только после
                                                    предварительной проверки нашими сотрудниками.
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="middleComentPopup">
                                                <!--                                            <form form="popupform" id="form-review">-->
                                                <input type="text" name="name" class="popupBtn" placeholder="Ваше имя"
                                                       id="input-name">
                                                <div class="blocRating">
                                                    <p class="blocRating__text">Ваша оценка</p>
                                                    <div id="reviewStars-input">
                                                        <input id="star-4" type="radio" name="rating" value="1"/>
                                                        <label title="gorgeous" for="star-4"></label>

                                                        <input id="star-3" type="radio" name="rating" value="2"/>
                                                        <label title="good" for="star-3"></label>

                                                        <input id="star-2" type="radio" name="rating" value="3"/>
                                                        <label title="regular" for="star-2"></label>

                                                        <input id="star-1" type="radio" name="rating" value="4"/>
                                                        <label title="poor" for="star-1"></label>

                                                        <input id="star-0" type="radio" name="rating" value="5"/>
                                                        <label title="bad" for="star-0"></label>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-md-4 padNone">
                                        <textarea name="text" id="comentspo" class="comentsPopup"
                                                  placeholder="Текст сообщения"></textarea>
                                            <button type="button" id="button-review">
                                                ДОБАВИТЬ СООБЩЕНИЕ
                                            </button>
                                        </div>
                                    </form>

                                </div>
                            </div>

                    </div>
                <?php } else { ?>
                    <?php echo $text_login; ?>
                <?php } ?>

                <div id="review"></div>

            </div>
        <?php } ?>

    </div>
</section>

<?php echo $content_bottom; ?>




<script>
    $('.navMainSlidershow li:first-child').addClass('active');
    $('.navMainSlidershow__aImg').click(handlerSliderShow);

</script>

<script type="text/javascript"><!--
    $('select[name=\'recurring_id\'], input[name="quantity"]').change(function () {
        $.ajax({
            url: 'index.php?route=product/product/getRecurringDescription',
            type: 'post',
            data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
            dataType: 'json',
            beforeSend: function () {
                $('#recurring-description').html('');
            },
            success: function (json) {
                $('.alert, .text-danger').remove();

                if (json['success']) {
                    $('#recurring-description').html(json['success']);
                }
            }
        });
    });
    //--></script>
<script type="text/javascript"><!--
    $('#button-cart').on('click', function () {
        $.ajax({
            url: 'index.php?route=checkout/cart/add',
            type: 'post',
            data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
            dataType: 'json',
            /*beforeSend: function () {
                $('#button-cart').button('loading');
            },
            complete: function () {
                $('#button-cart').button('reset');
            },*/
            success: function (json) {
                $('.alert, .text-danger').remove();
                $('.form-group').removeClass('has-error');

                if (json['error']) {
                    if (json['error']['option']) {
                        for (i in json['error']['option']) {
                            var element = $('#input-option' + i.replace('_', '-'));

                            if (element.parent().hasClass('input-group')) {
                                element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                            } else {
                                element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                            }
                        }
                    }

                    if (json['error']['recurring']) {
                        $('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
                    }

                    // Highlight any found errors
                    $('.text-danger').parent().addClass('has-error');
                }

                if (json['success']) {
                    $('.breadcrumb').after('<div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

//                    $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
                    $('#cart').html('<i class="clikMenu__circle">' + json['total_products'] + '</i>');

                    $('html, body').animate({scrollTop: 0}, 'slow');

//                    $('#cart > ul').load('index.php?route=common/cart/info ul li');
                    $('#cart_content').load('index.php?route=common/cart/info #cart_content');

                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });
    //--></script>
<script type="text/javascript"><!--
    $('.date').datetimepicker({
        pickTime: false
    });

    $('.datetime').datetimepicker({
        pickDate: true,
        pickTime: true
    });

    $('.time').datetimepicker({
        pickDate: false
    });

    $('button[id^=\'button-upload\']').on('click', function () {
        var node = this;

        $('#form-upload').remove();

        $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

        $('#form-upload input[name=\'file\']').trigger('click');

        if (typeof timer != 'undefined') {
            clearInterval(timer);
        }

        timer = setInterval(function () {
            if ($('#form-upload input[name=\'file\']').val() != '') {
                clearInterval(timer);

                $.ajax({
                    url: 'index.php?route=tool/upload',
                    type: 'post',
                    dataType: 'json',
                    data: new FormData($('#form-upload')[0]),
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function () {
                        $(node).button('loading');
                    },
                    complete: function () {
                        $(node).button('reset');
                    },
                    success: function (json) {
                        $('.text-danger').remove();

                        if (json['error']) {
                            $(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
                        }

                        if (json['success']) {
                            alert(json['success']);

                            $(node).parent().find('input').attr('value', json['code']);
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            }
        }, 500);
    });
    //--></script>
<script type="text/javascript"><!--
    $('#review').delegate('.pagination a', 'click', function (e) {
        e.preventDefault();

        $('#review').fadeOut('slow');

        $('#review').load(this.href);

        $('#review').fadeIn('slow');
    });

    $('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

    $('#button-review').on('click', function () {
        $.ajax({
            url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
            type: 'post',
            dataType: 'json',
            data: $("#form-review").serialize(),
            /*beforeSend: function () {
                $('#button-review').button('loading');
            },*/
            /*complete: function () {
                $('#button-review').button('reset');
            },*/
            success: function (json) {
                $('.alert-success, .alert-danger').remove();

                if (json['error']) {
                    $('#review-result').html('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
                }

                if (json['success']) {
                    $('#review-result').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

                    $('input[name=\'name\']').val('');
                    $('textarea[name=\'text\']').val('');
                    $('input[name=\'rating\']:checked').prop('checked', false);
                }
            }
        });
    });

    $(document).ready(function () {
        $('.thumbnails').magnificPopup({
            type: 'image',
            delegate: 'a',
            gallery: {
                enabled: true
            }
        });
    });
    //--></script>
<?php echo $footer; ?>
