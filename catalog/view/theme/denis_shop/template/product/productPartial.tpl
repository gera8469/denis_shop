<div class="row">
    <div class="col-md-6">
        <div class="blocSlidershow">
            <div class="mainBlocImg">
                <a href="#" class="mainBlocImg__plus">+</a>
                <img src="<?= $product['thumb']?>" alt="<?= $product['name']?>">
            </div>
                <?php if ($product['thumb'] || $product['images']) { ?>
                    <ul class="navMainSlidershow">

                        <?php if ($product['thumb']) { ?>
                            <li class="navMainSlidershow__aImg">
                                <a href="#">
                                    <img src="<?php echo $product['thumb']; ?>" alt="<?=$product['name']?>">
                                </a>
                            </li>
                        <?php } ?>
                        <?php if ($product['images']) { ?>
                            <?php foreach ($product['images'] as $image) { ?>
                                <li class="navMainSlidershow__aImg">
                                    <a href="#">
                                        <img src="<?php echo $image['thumb']; ?>" alt="<?=$product['name']?>">
                                    </a>
                                </li>
                            <?php } ?>
                        <?php } ?>
                    </ul>
                <?php } ?>
        </div>
    </div>
    <div class="col-md-6 padNo">
        <div class="blocOpItem">
            <span class="blocOpItem__popular">Popular</span>
            <h3 class="blocCategories-h2 blocCategories-h2--item">
                <span><?=$product['name']?></span>Denis
            </h3>
            <div class="wrapLinearBlack wrapLinearBlack--pad wrapLinearBlack--padItem">
                <div class="wrapLinearBlack__bold"></div>
            </div>
            <div class="options options--border">
                <p class="options__text">
                    <span>Модель:</span><?= $product['model']?>
                </p>
                <p class="options__text">
                    <span>Наличие:</span><?=$product['stock_status']?>
                </p>
                <p class="options__text2">
                    <?= $product['description']?>
                </p>
            </div>
            <?php if ($product['attribute_groups']) { ?>
                <div class="tab-pane" id="tab-specification">
                    <table class="table table-bordered">
                        <?php foreach ($product['attribute_groups'] as $attribute_group) { ?>
                            <thead>
                            <tr>
                                <td colspan="2"><strong><?php echo $attribute_group['name']; ?></strong>
                                </td>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                                <tr>
                                    <td><?php echo $attribute['name']; ?></td>
                                    <td><?php echo $attribute['text']; ?></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        <?php } ?>
                    </table>
                </div>
            <?php } ?>
            <div id="product">
                <?php if (!empty($product['options'])) { ?>
                    <?php foreach ($product['options'] as $option) { ?>
                        <?php if ($option['type'] == 'select') { ?>
                            <div class="<?php echo($option['required'] ? ' required' : ''); ?>">
                                <label class="" for="input-option<?php echo $option['product_option_id']; ?>">
                                    <?php echo $option['name']; ?>
                                </label>
                                <select name="option[<?php echo $option['product_option_id']; ?>]"
                                        id="input-option<?php echo $option['product_option_id']; ?>"
                                        class="form-control">
                                    <option value="">
                                        <?php echo $text_select; ?>
                                    </option>
                                    <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                        <option value="<?php echo $option_value['product_option_value_id']; ?>">
                                            <?php echo $option_value['name']; ?>
                                            <?php if ($option_value['price']) { ?>
                                                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                            <?php } ?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </div>
                        <?php } ?>
                        <?php if ($option['type'] == 'radio') { ?>
                            <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
                                <label class="control-label"><?php echo $option['name']; ?></label>
                                <div id="input-option<?php echo $option['product_option_id']; ?>">
                                    <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                        <div class="radio">
                                            <label>
                                                <input type="radio"
                                                       name="option[<?php echo $option['product_option_id']; ?>]"
                                                       value="<?php echo $option_value['product_option_value_id']; ?>"/>
                                                <?php echo $option_value['name']; ?>
                                                <?php if ($option_value['price']) { ?>
                                                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                <?php } ?>
                                            </label>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        <?php } ?>
                        <?php if ($option['type'] == 'checkbox') { ?>
                            <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
                                <label class="control-label"><?php echo $option['name']; ?></label>
                                <div id="input-option<?php echo $option['product_option_id']; ?>">
                                    <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox"
                                                       name="option[<?php echo $option['product_option_id']; ?>][]"
                                                       value="<?php echo $option_value['product_option_value_id']; ?>"/>
                                                <?php echo $option_value['name']; ?>
                                                <?php if ($option_value['price']) { ?>
                                                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                <?php } ?>
                                            </label>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        <?php } ?>
                        <?php if ($option['type'] == 'image') { ?>
                            <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
                                <label class="control-label"><?php echo $option['name']; ?></label>
                                <div id="input-option<?php echo $option['product_option_id']; ?>">
                                    <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                        <div class="radio">
                                            <label>
                                                <input type="radio"
                                                       name="option[<?php echo $option['product_option_id']; ?>]"
                                                       value="<?php echo $option_value['product_option_value_id']; ?>"/>
                                                <img src="<?php echo $option_value['image']; ?>"
                                                     alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>"
                                                     class="img-thumbnail"/> <?php echo $option_value['name']; ?>
                                                <?php if ($option_value['price']) { ?>
                                                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                <?php } ?>
                                            </label>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        <?php } ?>
                        <?php if ($option['type'] == 'text') { ?>
                            <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
                                <label class="control-label"
                                       for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                <input type="text" name="option[<?php echo $option['product_option_id']; ?>]"
                                       value="<?php echo $option['value']; ?>"
                                       placeholder="<?php echo $option['name']; ?>"
                                       id="input-option<?php echo $option['product_option_id']; ?>"
                                       class="form-control"/>
                            </div>
                        <?php } ?>
                        <?php if ($option['type'] == 'textarea') { ?>
                            <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
                                <label class="control-label"
                                       for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                <textarea name="option[<?php echo $option['product_option_id']; ?>]" rows="5"
                                          placeholder="<?php echo $option['name']; ?>"
                                          id="input-option<?php echo $option['product_option_id']; ?>"
                                          class="form-control"><?php echo $option['value']; ?></textarea>
                            </div>
                        <?php } ?>
                        <?php if ($option['type'] == 'file') { ?>
                            <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
                                <label class="control-label"><?php echo $option['name']; ?></label>
                                <button type="button"
                                        id="button-upload<?php echo $option['product_option_id']; ?>"
                                        data-loading-text="<?php echo $text_loading; ?>"
                                        class="btn btn-default btn-block"><i
                                            class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
                                <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]"
                                       value="" id="input-option<?php echo $option['product_option_id']; ?>"/>
                            </div>
                        <?php } ?>
                        <?php if ($option['type'] == 'date') { ?>
                            <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
                                <label class="control-label"
                                       for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                <div class="input-group date">
                                    <input type="text"
                                           name="option[<?php echo $option['product_option_id']; ?>]"
                                           value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD"
                                           id="input-option<?php echo $option['product_option_id']; ?>"
                                           class="form-control"/>
                                    <span class="input-group-btn">
                <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                </span></div>
                            </div>
                        <?php } ?>
                        <?php if ($option['type'] == 'datetime') { ?>
                            <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
                                <label class="control-label"
                                       for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                <div class="input-group datetime">
                                    <input type="text"
                                           name="option[<?php echo $option['product_option_id']; ?>]"
                                           value="<?php echo $option['value']; ?>"
                                           data-date-format="YYYY-MM-DD HH:mm"
                                           id="input-option<?php echo $option['product_option_id']; ?>"
                                           class="form-control"/>
                                    <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span></div>
                            </div>
                        <?php } ?>
                        <?php if ($option['type'] == 'time') { ?>
                            <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
                                <label class="control-label"
                                       for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                <div class="input-group time">
                                    <input type="text"
                                           name="option[<?php echo $option['product_option_id']; ?>]"
                                           value="<?php echo $option['value']; ?>" data-date-format="HH:mm"
                                           id="input-option<?php echo $option['product_option_id']; ?>"
                                           class="form-control"/>
                                    <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span></div>
                            </div>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>
                <div class="blocPrice blocPrice--pad">
                    <?php if (!$product['special']) { ?>
                        <p class="blocPrice__price">
                            &#8372;<?php echo round($product['price']); ?>
                        </p>
                    <?php } else { ?>
                        <p class="blocPrice__price">
                            &#8372;<?php echo round($product['special']); ?>
                            <span>&#8372;<?php echo round($product['price']); ?></span>
                        </p>
                    <?php } ?>

                    <div class="wrapBtn">
                        <button class="wrapBtn__left" onclick="productHandler.changeQuantityValue(-1, <?php echo $product['minimum']; ?>)">-</button>
                        <input type="text" class="wrapBtn__texInp" name="quantity" value="<?php echo $product['minimum']; ?>" size="2"
                               id="input-quantity" class="form-control">
                        <button class="wrapBtn__right" onclick="productHandler.changeQuantityValue(1, <?php echo $product['minimum']; ?>)">+</button>
                        <input type="hidden" name="product_id" value="<?php echo $product['product_id']; ?>"/>
                    </div>
                    <div class="btnLike">
                        <button ><i class="fa fa-heart" aria-hidden="true" onclick="productHandler.addToWishList('<?php echo $product['product_id']; ?>');"></i></button>
                        <!--<button type="button" data-toggle="tooltip" class="btn btn-default"
                                title="<?php /*echo $button_wishlist; */?>"
                                onclick="wishlist.add('<?php /*echo $product_id; */?>');"><i class="fa fa-heart"></i>
                        </button>-->
                    </div>

                </div>
            </div>



            <br/>
            <button type="button" id="button-cart" class="submitIte">
                ДОБАВИТЬ В КОРЗИНУ
            </button>

            <!--<div class="blocFormItem">
                <form action="#" class="formItem">
                    <div class="blocFormItem__select">
                        <label for="selform">Емкость</label>
                        <select name="sel" id="selform">
                            <option value="100">100 мл.</option>
                            <option value="200">200 мл.</option>
                            <option value="500">500 мл.</option>
                        </select>
                    </div>
                    <div class="blocFormItem__color colorItem">
                        <span class="colorItem__name">Цвет</span>
                        <div class="colorItem__blockColor ">
                            <div class="js-color colorItem__blockColor--red"><p>Красный / эмаль</p>
                            </div>
                            <div class="js-color colorItem__blockColor--red colorItem__blockColor--blue">
                                <p>Синий / эмаль</p></div>
                            <div class=" js-color colorItem__blockColor--red colorItem__blockColor--green">
                                <p>Зеленый / эмаль</p></div>
                            <div class="js-color colorItem__blockColor--red colorItem__blockColor--feol">
                                <p>Феолетовый / эмаль</p></div>
                            <div class="js-color colorItem__blockColor--red colorItem__blockColor--yellow">
                                <p>Желтый / эмаль</p></div>
                        </div>
                        <span class="colorItem__kol">Красный / эмаль</span>
                    </div>
                    <div class="blocPrice blocPrice--pad">
                        <p class="blocPrice__price">
                            &#8372; 154.00
                        </p>
                        <div class="wrapBtn">
                            <button class="wrapBtn__left">-</button>
                            <input type="text" class="wrapBtn__texInp" placeholder="1">
                            <button class="wrapBtn__right">+</button>
                        </div>
                        <div class="btnLike">
                            <a href="#"><i class="fa fa-heart" aria-hidden="true"></i></a>
                        </div>

                    </div>
                    <button class="submitIte">ДОБАВИТЬ В КОРЗИНУ</button>
<!--                </form>-->
                <p class="options__text2">
                    Доставка товара по Киеву - бесплатно курьером. В другие города Украины -
                    отправка товара по Новой Почте наложенным платежем. При заказе от 1500 грн
                    доставка будет бесплатной в любой регион. Также принимайте участие в
                    клиентской программе DENIS CLUB, чтобы получать скидки, подарки,
                    дисконтную карту.
                </p>
            </div>
        </div>
    </div>
</div>

<script>
    $('.navMainSlidershow li:first-child').addClass('active');
    $('.navMainSlidershow__aImg').click(handlerSliderShow);

</script>

<script type="text/javascript"><!--
    $('#button-cart').on('click', function () {
        $.ajax({
            url: 'index.php?route=checkout/cart/add',
            type: 'post',
            data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
            dataType: 'json',
            beforeSend: function () {
                $('#button-cart').button('loading');
            },
            complete: function () {
                $('#button-cart').button('reset');
            },
            success: function (json) {
                $('.alert, .text-danger').remove();
//                $('.form-group').removeClass('has-error');

                if (json['error']) {
                    if (json['error']['option']) {
                        for (i in json['error']['option']) {
                            var element = $('#input-option' + i.replace('_', '-'));

                            if (element.parent().hasClass('input-group')) {
                                element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                            } else {
                                element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                            }
                        }
                    }

                    if (json['error']['recurring']) {
                        $('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
                    }

                    // Highlight any found errors
                    $('.text-danger').parent().addClass('has-error');
                }

                if (json['success']) {
//                    $('.breadcrumb').after('<div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

//                    $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
                    $('#cart').html('<i class="clikMenu__circle">' + json['total_products'] + '</i>');

//                    $('html, body').animate({scrollTop: 0}, 'slow');

//                    $('#cart > ul').load('index.php?route=common/cart/info ul li');
                    $('#cart_content').load('index.php?route=common/cart/info #cart_content');

                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });
    //--></script>