<?php if ($reviews) { ?>
    <?php foreach ($reviews as $review) { ?>
        <div class="wrapComents">
            <div class="row">
                <div class="col-md-2">
                    <div class="wrapComents__left author">
                        <p class="author__name"><?php echo $review['author']; ?>, <?= $review['city']?></p>
                        <p class="author__datePosts"><?php echo $review['date_added']; ?></p>
                        <ul class="author__stars">
                            <?php for ($i = 1; $i <= 5; $i++) {

                                $ratingClass = '';

                                if ($review['rating'] < $i) {
                                    $ratingClass = 'star-star1';
                                } else {
                                    $ratingClass = 'star-star3';
                                } ?>
                                <div class="<?= $ratingClass ?>"></div>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
                <div class="col-md-10">
                    <p class="textComent"><?php echo $review['text']; ?></p>
                </div>
            </div>
        </div>
    <?php } ?>
    <div class="text-right"><?php echo $pagination; ?></div>
<?php } else { ?>
    <p><?php echo $text_no_reviews; ?></p>
<?php } ?>
