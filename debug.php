<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 14.07.17
 * Time: 12:49
 */
/**
 * @param $value
 * @param bool $toJson
 * @param bool $toExit
 */
function myDump($value, $toJson = true, $toExit = true){
    if ($toJson) {
        echo json_encode($value);
    } else {
        echo '<pre>';
        var_dump($value);
        echo '</pre>';
    }

    if ($toExit) {
        exit();
    }
}

function arrayDump($value, array $options = [], $toExit = true)
{

    if (empty($options)) {
        myDump($value, false, $toExit);
    } else {
        echo '<pre>';
        foreach ($value as $item) {
            echo '<hr>';
            foreach ($options as $option) {
                var_dump($item[$option]);
            }
        }
        echo '</pre>';
    }

    if ($toExit) {
        exit();
    }
}